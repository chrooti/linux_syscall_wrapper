#!/usr/bin/env -S python3 -B

"""
Simplistic template engine, acts differently based on the first characters in a line.

Rules:
- single % -> output the line verbatim as code
- double %% -> output a single % and treat the line as normal text
- none of the above -> treat line as normal text
- if a line that starts with "%" ends with "%" the indentation is increased by one starting from the next line.

String interpolation rules:
- ${value} -> inserted in an f-string as {value}, *must not contain curly brackets*
- $${value} -> output as ${value}
- you must not escape either "$", "{" or "}" in any other case

Example: ```
% for fruit in fruits:
    % is_pear = fruit == "pear"
    % if is_pear:
${fruit} is pear!
    % else
${fruit} isn't pear!
    % # endif
% # endfor
```

Note that "%" after the for/if blocks is needed to reset the indentation, but the comments afterwards are just for clarity.
"""

import os.path
import re
import typing as t


class Template:
    INDENTATION_UNIT = 4
    TAB = INDENTATION_UNIT * " "

    def __init__(self, string: str):
        self.lines = string.splitlines()
        self._body: list[str] = []
        self._current_indentation_units = 0

    def emit(self) -> str:
        # preamble
        self._add_line("__lines = []")

        # body
        for line in self.lines:
            stripped_line = line.lstrip()
            indentation = len(line) - len(stripped_line)

            if not stripped_line.startswith("%"):
                self._add_text_line(line)

            elif len(stripped_line) <= 1:
                self._set_indentation(indentation)
                self._body.append("")

            elif stripped_line[1] == "%":
                self._add_text_line(indentation * " " + stripped_line[1:])

            else:
                self._set_indentation(indentation)
                self._add_code_line(stripped_line[1:].strip())

        # epilogue
        self._add_line('__result = "\\n".join(__lines)')

        return "\n".join(self._body)

    @classmethod
    def evaluate(cls, string: str, **kwargs: t.Any) -> str:
        """
        Entrypoint:
        - parses the template
        - returns the result of the evaluation with the given kwargs
        """
        template_fn = cls(string).emit()

        namespace = {**kwargs}
        exec(template_fn, namespace, namespace)

        return t.cast(str, namespace["__result"])

    def _add_text_line(self, line: str) -> None:
        line = (
            line
            # escape backslash
            .replace("\\", "\\\\")
            # escape string delimiter
            .replace('"', '\\"')
            # escape brackets for f-strings
            .replace("{", "{{").replace("}", "}}")
        )

        # match the `${}` sections and rewrite them
        # note that due to the previous substitution
        # we must work with e.g. "{{" instead of "{"
        line = re.sub(
            r"(?P<dollar_escape>\$)?"
            r"(?P<opening_sequence>\$\{\{)"
            r"(?P<content>[^{}]*?)"
            r"(?P<closing_sequence>\}\})",
            self._replace_interpolation_sequences,
            line,
        )

        self._add_line(f'__lines.append(f"{line}")')

    def _add_code_line(self, line: str) -> None:
        if line.endswith("%"):
            line = line[:-1].rstrip()
            self._add_line(line)
            self._current_indentation_units += 1
        else:
            self._add_line(line)

    def _add_line(self, line: str) -> None:
        whitespace = self._current_indentation_units * self.INDENTATION_UNIT * " "

        self._body.append(f"{whitespace}{line}")

    def _set_indentation(self, indentation: int) -> None:
        units, rem = divmod(indentation, self.INDENTATION_UNIT)

        assert rem % self.INDENTATION_UNIT == 0
        self._current_indentation_units = units

    @staticmethod
    def _replace_interpolation_sequences(match: re.Match) -> str:
        groups = match.groupdict()
        content = groups["content"]

        if groups["dollar_escape"]:
            # if the token is escaped return it minus the first "$" but plus the doubled brackets
            # e.g. "${{content}}"
            return f"${{{{{content}}}}}"
        else:
            # return the token as interpolable in the f-string
            # e.g. "{content}"
            return f"{{{content}}}"


def main() -> None:
    import getopt
    import json
    import sys

    options, _ = getopt.gnu_getopt(
        sys.argv[1:],
        "",
        ["json=", "template="],
    )

    template_file = None
    json_file = None
    for flag, value in options:
        if flag == "--json":
            json_file = value

        elif flag == "--template":
            template_file = value

    assert json_file is not None
    assert template_file is not None

    with open(json_file, "r") as f:
        context = json.load(f)

    with open(template_file, "r") as f:
        template_string = f.read()

    output = Template.evaluate(
        template_string,
        __file__=os.path.abspath(template_file),
        **context,
    )

    print(output, flush=True)


if __name__ == "__main__":
    main()
