#!/usr/bin/env bash

set -o pipefail
shopt -s globstar nullglob extglob

SCRIPT_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
TEMPLATE_DIR="$SCRIPT_DIR/../templates"
TEMPLATE="$SCRIPT_DIR/template.py"

# must be implemented in assembly
SKIPPED_SYSCALLS="fork,vfork,clone,clone3"

prepare() {
    make distclean
    make CC=clang defconfig
    make CC=clang -j "$(nproc)" vmlinux
    ./scripts/clang-tools/gen_compile_commands.py
}

find_implementation_files() {
    local directories
    mapfile -t directories < <(
        find . \
            -maxdepth 1 \
            -type d \
            -not -name '.*' \
            -not -name 'LICENSES' \
            -not -name 'Documentation' \
            -not -name 'arch' \
            -printf '%P\n'
    )

    OIFS="$IFS"
    IFS=','

    rg \
        -l \
        -g '{'"${directories[*]}"'}/**/*.c' \
        -g 'arch/'"$1"'/**/*.c' \
        '^(?:COMPAT_)?SYSCALL_DEFINE[0-9]\([a-zA-Z0-9_]+[,)]' |
        cat - <(echo kernel/sched/build_utility.c) |
        sort -u

    IFS="$OIFS"
}

find_x86_64() {
    local implementation_files
    mapfile -t implementation_files < <(find_implementation_files x86)

    local syscalls
    read -r syscalls < <(
        lsw \
            -table 'arch/x86/entry/syscalls/syscall_64.tbl' \
            -arch common,64 \
            -skipped "${SKIPPED_SYSCALLS}" \
            "${implementation_files[@]}"
    )

    local vdso_syscall_names
    mapfile -t vdso_syscall_names < <(
        rg \
            -oNI \
            --no-heading \
            -r '$1' \
            '^\s+__vdso_([a-zA-Z0-9_]+);' \
            arch/x86/entry/vdso/vdso.lds.S |
            sort -u |
            xargs -I% echo \"%\"
    )

    OIFS="$IFS"
    IFS=","

    local output
    read -r -d $'\0' output <<- EOF
    {
        "arch": "x86_64",
        "syscalls": $syscalls,
        "vdso_syscall_names": [${vdso_syscall_names[*]}]
    }
EOF

    IFS="$OIFS"

    echo "$output"
}

_emit_cpp() {
    local namespace="$2"
    [[ -z $namespace ]] && namespace="syscall"

    local arch="$3"

    local data
    read -r -d $'\0' data < "$1"

    local config
    read -r -d $'\0' config <<- EOF
    {
        "emit_module": $EMIT_MODULE,
        "namespace": "$namespace",
        "data": $data
    }
EOF

    "$TEMPLATE" \
        --template "$TEMPLATE_DIR"/syscall.cpp.tmpl \
        --json <(echo "$config")
}

emit_cpp() {
    EMIT_MODULE=false
    _emit_cpp "$@"
}

emit_cppm() {
    EMIT_MODULE=true
    _emit_cpp "$@"
}

main() {
    case "$1" in
    prepare)
        prepare
        ;;
    find)
        local arch="$2"
        find_"$arch"
        ;;
    emit)
        local language="$2"
        local json_path="$3"
        shift 3
        emit_"$language" "$json_path" "$@"
        ;;
    *)
        echo "Unrecognized command"
        ;;
    esac
}

main "$@"
