#!/usr/bin/env -S python3 -B

import contextlib
import os
import os.path
import shlex
import shutil
import subprocess
import sys

script_dir = os.path.dirname(os.path.realpath(__file__))

output_base_dir = "out"
object_dir = f"{output_base_dir}/objects"
pcm_dir = f"{output_base_dir}/modules"
config_dir = f"{output_base_dir}/config"
dyndep_file = f"{config_dir}/modules.dd"
module_deps_file = f"{output_base_dir}/module_deps.json"
header_deps_file = f"{output_base_dir}/header_deps.d"


def getenv_realpath(key: str, fallback: str) -> str:
    return os.path.realpath(os.getenv(key, fallback), strict=True)

def ensure_output_base_dir() -> None:
    os.makedirs(output_base_dir, exist_ok=True)

def gen_config() -> None:
    cc = getenv_realpath("CC", "/usr/bin/clang++")
    clang_scan_deps = getenv_realpath("CLANG_SCAN_DEPS", "/usr/bin/clang-scan-deps")
    p1689_to_ninja = getenv_realpath("P1689_TO_NINJA", f"{script_dir}/p1689_to_ninja.py")

    cflags: list[str] = [f"-fprebuilt-module-path={pcm_dir}"]
    ldflags: list[str] = []

    debug_cflags = (
        "-O1",
        "-g",
        "-fno-optimize-sibling-calls",
        "-fno-omit-frame-pointer",
    )

    SANITIZE = os.getenv("SANITIZE")
    DEBUG = os.getenv("DEBUG")

    if SANITIZE == "memory":
        cflags.extend((
            *debug_cflags,
            "-fsanitize=memory",
            "-fsanitize-memory-track-origins",
            "-fsanitize-recover=all",
        ))

        ldflags.extend((
            "-g",
            "-fsanitize-memory",
            "-fsanitize-recover=all"
        ))
    elif SANITIZE == "address":
        cflags.extend((
            *debug_cflags,
            "-fsanitize=address,undefined,integer,nullability"
            "-fsanitize-address-use-after-scope"
            "-fsanitize-recover=all"
        ))

        ldflags.extend((
            "-g",
            "-fsanitize=address,undefined,integer,nullability",
            "-fsanitize-recover=all"
        ))
    elif DEBUG == "1":
        cflags.extend(debug_cflags)
    else:
        cflags.append("-DNDEBUG")

    if os.getenv("COVERAGE") == "1":
        cflags.extend((
            "-fprofile-instr-generate",
            "-fcoverage-mapping",
        ))

        ldflags.extend((
            "-fprofile-instr-generate",
            "-fcoverage-mapping",
        ))

    if os.getenv("PEDANTIC") == "1":
        cflags.extend((
            "-Werror",
            "-Wpedantic",
            "-pedantic-errors",
        ))

    if external_cflags := os.getenv("CFLAGS"):
        cflags.extend(shlex.split(external_cflags))

    if external_cxxflags := os.getenv("CXXFLAGS"):
        cflags.extend(shlex.split(external_cxxflags))

    if external_ldflags := os.getenv("LDFLAGS"):
        ldflags.extend(shlex.split(external_ldflags))

    output = '\n'.join((
        f"CC = {cc}",
        f"CLANG_SCAN_DEPS = {clang_scan_deps}",
		f"P1689_TO_NINJA = {p1689_to_ninja}",
		f"CFLAGS = ${{CFLAGS}} {' '.join(cflags)}",
		f"LDFLAGS = ${{LDFLAGS}} {' '.join(ldflags)}",
        ""
    ))

    with open(f"{output_base_dir}/config.ninja", "w", encoding="utf-8") as f:
        f.write(output)

def gen_targets() -> None:
    targets: list[str] = []
    depfiles = []

    object_suffixes = {".cpp", ".cc", ".cxx", ".c++"}
    module_suffixes = set(f"{suffix}m" for suffix in object_suffixes)

    for source_root in sys.argv[1:]:
        objects = []

        for root, _dirs, files in os.walk(source_root):
            for file in files:
                source = f"{root}/{file}"
                source_full_name, ext = os.path.splitext(source)
                object_path = f"{object_dir}/{source_full_name}.o"

                if ext in module_suffixes:
                    source_name = os.path.basename(source_full_name)
                    pcm_path = f"{pcm_dir}/{source_name}.pcm"

                    depfile_path = f"{pcm_path}.d"
                    depfiles.append(depfile_path)

                    targets.extend((
                        f"build {pcm_path}: ccm {source} || {dyndep_file}",
                        f"    dyndep = {dyndep_file}",
                        f"build {object_path}: ccpcm {pcm_path}",
                        f"build {depfile_path}: write_empty_file",
                        "",
                    ))
                    objects.append(object_path)

                elif ext in object_suffixes:
                    depfile_path = f"{object_path}.d"
                    depfiles.append(depfile_path)

                    targets.extend((
                        f"build {object_path}: cc {source} || {dyndep_file}",
                        f"    dyndep = {dyndep_file}",
                        f"build {depfile_path}: write_empty_file",
                        "",
                    ))
                    objects.append(object_path)

                else:
                    continue

        executable_name = os.path.basename(source_root)
        executable_path = f"{output_base_dir}/{executable_name}"

        targets.extend((
            f"build {executable_path}: link {' '.join(objects)}",
            f"build {executable_name}: phony {executable_path}",
            f"default {executable_path}",
            ""
        ))

    depfiles_str = " $\n".join(depfiles)

    targets.extend((
        f"build {dyndep_file}: p1689_to_ninja {module_deps_file}",
        f"build {module_deps_file}: scan_module_dependencies compile_commands.json || {depfiles_str}",
        f""
    ))

    with open(f"{output_base_dir}/targets.ninja", "w", encoding="utf-8") as f:
        f.write('\n'.join(targets))

def clean() -> None:
    shutil.rmtree(output_base_dir, ignore_errors=True)

    with contextlib.suppress(FileNotFoundError):
        os.remove("compile_commands.json")

def regen() -> None:
    gen_targets()
    with open("compile_commands.json", mode="w", encoding="utf-8") as compile_commands:
        subprocess.run(
            ["ninja", "-t", "compdb", "cc", "ccm"],
            stdout=compile_commands
        )

def main() -> None:
    task = sys.argv.pop(1)

    match task:
        case "setup":
            clean()
            ensure_output_base_dir()
            gen_config()
            regen()
        case "reconfig":
            gen_config()
        case "regen":
            regen()
        case "clean":
            clean()
        case _:
            print(f"unknown task {task}")
            exit(1)

main()
