#!/usr/bin/env -S python3 -B

import json
import os.path
import sys
import typing as t


ProvidedModule = t.TypedDict(
    "ProvidedModule",
    {
        "source-path": str,
        "compiled-module-path": str,
        "unique-on-source-path": bool,  # default: False
        "logical-name": str,  # required
        "is-interface": bool,  # default: True
    },
)

RequiredModule = t.TypedDict(
    "RequiredModule",
    {
        "source-path": str,
        "compiled-module-path": str,
        "unique-on-source-path": bool,  # default: False
        "logical-name": str,  # required
        "lookup-method": (
            t.Literal["by-name"]
            | t.Literal["include-angle"]
            | t.Literal["include-quote"]
        ),  # default: "by-name"
    },
)

Rule = t.TypedDict(
    "Rule",
    {
        "work-directory": str,
        "primary-output": str,  # required (not by spec)
        "outputs": list[str],
        "provides": list[ProvidedModule],  # default: []
        "requires": list[RequiredModule],  # default: []
    },
)

Dependencies = t.TypedDict(
    "Dependencies", {"version": int, "revision": int, "rules": list[Rule]}
)


class CompileUnit:
    def __init__(
        self,
        *,
        primary_output: str,
        outputs: list[str],
        requires: list[str],
    ):
        self.primary_output = primary_output
        self.outputs = outputs
        self.requires = requires


def parse_dependencies(
    dependencies: Dependencies,
) -> tuple[list[CompileUnit], dict[str, str]]:
    compile_units = []
    provides = {}

    for rule in dependencies["rules"]:
        base_path = rule.get("work-directory", "")
        primary_output = os.path.join(base_path, rule["primary-output"])

        outputs = [
            os.path.join(base_path, output) for output in rule.get("outputs", [])
        ]
        requires = [require["logical-name"] for require in rule.get("requires", [])]
        compile_unit = CompileUnit(
            primary_output=primary_output,
            outputs=outputs,
            requires=requires,
        )
        compile_units.append(compile_unit)

        for provide in rule.get("provides", []):
            provides[provide["logical-name"]] = primary_output

    return compile_units, provides


def generate_rules(
    compile_units: list[CompileUnit],
    provides: dict[str, str],
    *,
    prologue: list[str] | None = None,
    before_main_output: str = "",
    before_other_outputs: str = "",
    separator: str = "",
    before_requires: str = "",
) -> list[str]:
    rules = [] if not prologue else prologue

    for compile_unit in compile_units:
        rule_string = f"{before_main_output}{compile_unit.primary_output}"
        if compile_unit.outputs:
            rule_string += f" {before_other_outputs}{' '.join(compile_unit.outputs)}"

        rule_string += separator
        if compile_unit.requires:
            file_requires = [provides[require] for require in compile_unit.requires]
            rule_string += f" {before_requires}{' '.join(file_requires)}"

        rules.append(rule_string)

    return rules


def generate_ninja_rules(
    compile_units: list[CompileUnit],
    provides: dict[str, str],
) -> list[str]:
    return generate_rules(
        compile_units,
        provides,
        prologue=["ninja_dyndep_version = 1"],
        before_main_output="build ",
        before_other_outputs="| ",
        separator=": dyndep",
        before_requires="| "
    )


def generate_make_rules(
    compile_units: list[CompileUnit],
    provides: dict[str, str],
) -> list[str]:
    return generate_rules(
        compile_units,
        provides,
        before_main_output="",
        separator=":",
    )


def main() -> None:
    with open(sys.argv[1], encoding="utf-8", mode="r") as f:
        dependencies: Dependencies = json.load(f)

    compile_units, provides = parse_dependencies(dependencies)
    rules = generate_ninja_rules(compile_units, provides)
    rules.append("")

    sys.stdout.write("\n".join(rules))


main()
