#include <stdio.h>

#include <stdint.h>
#include <linux/auxvec.h>
#include <linux/elf.h>

struct AuxvEntry {
    uint64_t type;
    uint64_t value;
};

unsigned char* vdso_addr;
char* vdso_dynstr = nullptr;
Elf64_Sym* vdso_dynsym = nullptr;
size_t vdso_dynsym_entries = 0;

bool elf_strncmp(const char* s1, const char *s2, size_t s2_len) {
    for(size_t i = 0; i < s2_len; i++) {
        if(s1[i] == '\0') {
            return false;
        }

        if(s1[i] != s2[i]) {
            return false;
        }
    }

    return true;
}

#define ELF_STRNCMP(s1, s2) elf_strncmp(s1, s2, sizeof(s2) - 1)

void vdso_setup(int argc, char** argv) {
    char** envp = argv + argc + 1;
    while(*envp != nullptr) {
        ++envp;
    }

    AuxvEntry* auxval = reinterpret_cast<AuxvEntry*>(envp + 1);
    while(auxval->type != AT_SYSINFO_EHDR) {
        auxval++;
    }
    vdso_addr = reinterpret_cast<unsigned char*>(auxval->value);

    auto* elf_header = reinterpret_cast<Elf64_Ehdr*>(vdso_addr);
    auto* section_headers = reinterpret_cast<Elf64_Shdr*>(
        vdso_addr + elf_header->e_shoff
    );
    Elf64_Shdr& string_section_header = section_headers[elf_header->e_shstrndx];

    for (int i=0; i<elf_header->e_shnum; i++) {
        Elf64_Shdr& header = section_headers[i];
        auto* name = reinterpret_cast<char*>(
            vdso_addr + string_section_header.sh_offset + header.sh_name
        );
        if (ELF_STRNCMP(name, ".dynstr")) {
            vdso_dynstr = reinterpret_cast<char*>(vdso_addr + header.sh_offset);
        }
        if (ELF_STRNCMP(name, ".dynsym")) {
            vdso_dynsym = reinterpret_cast<Elf64_Sym*>(vdso_addr + header.sh_offset);
            vdso_dynsym_entries = header.sh_size / header.sh_entsize;
        }
    }
}

void* vdso_sym(const char* sym_name, size_t sym_name_len) {
    void *ret = NULL;

    for (int i = 0; i < vdso_dynsym_entries; i++) {
        Elf64_Sym& sym = vdso_dynsym[i];
        char* name = vdso_dynstr + sym.st_name;
        if (elf_strncmp(name, sym_name, sym_name_len)) {
            ret = (vdso_addr + sym.st_value);
            break;
        }
    }

    return ret;
}

#include <time.h>  // for clockid_t, timespec, and CLOCK_REALTIME
typedef int (clock_gettime_t)(clockid_t clk_id, struct timespec *tp);

int main(int argc, char **argv) {
    vdso_setup(argc, argv);
    auto my_clock_gettime = (clock_gettime_t*) vdso_sym("clock_gettime", sizeof("clock_gettime") - 1);

    timespec tp;
    my_clock_gettime(CLOCK_REALTIME, &tp);
    printf("tp: %ld", tp.tv_sec);

	return 0;
}
