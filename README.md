# Linux Syscall Wrapper

C++ syscall interface generator for Linux.

## Use a prebuilt header

- Include the generated syscall.hpp (x86 only for now)

## Build the header yourself

Requirements:
- ripgrep
- python
- bash
- the lsw executable in your path
- the Linux kernel source
- a working clang

How to (example for C++ and x86_64):
- cd into the Linux source directory
- Run
```bash
wrapper.sh prepare
wrapper.sh find x86_64 1> syscalls.json
wrapper.sh emit cpp syscalls.json 1> syscall.hpp
```

## Build LSW from source

Additional requirements:
- ninja
- clang

How to:
```bash
./tasks setup
./gen_ninja_config.sh
./tasks build
```

## Laundry list of missing things:

- implement fork/vfork/clone3 in asm
- support C (rust? zig?)
- at least ARM support

```
podman run -it --rm --volume "$(pwd)":"$(pwd)" --workdir "$(pwd)" llvm:16

CC=/usr/bin/clang++-16 CLANG_SCAN_DEPS=/usr/bin/clang-scan-deps-16 ./scripts/shuriken.py setup lsw
ninja -d keepdepfile out/precompiled/lsw/Precompiled.pch
ninja -d keepdepfile
```
