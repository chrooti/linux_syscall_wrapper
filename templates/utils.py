# mypy: disable-error-code=no-untyped-def

class Object:
    def __contains__(self, key):
        return key in self.__dict__

    def __getitem__(self, attr):
        return self.__dict__.get(attr)

    def __iter__(self):
        for key, value in self.__dict__.items():
            if key.startswith("__"):
                continue
            yield key, value

def convert_value(value):
    if isinstance(value, dict):
        converted_value = Object()
        for key, subvalue in value.items():
            setattr(converted_value, key, convert_value(subvalue))

        return converted_value
    elif isinstance(value, list):
        return [convert_value(subvalue) for subvalue in value]
    else:
        return value


def arg2obj(_globals):
    for name, value in _globals.items():
        if name.startswith("__"):
            continue
        _globals[name] = convert_value(value)
