#include "Precompiled.hpp"

import lsw.Commons;
import lsw.FindEntrypoints;
import lsw.Output;
import lsw.ParseTable;

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

static cl::OptionCategory LSWToolCategory("lsw");

static cl::opt<std::string> SyscallTableFile(cl::cat(LSWToolCategory), "table",
                                             cl::Required,
                                             cl::desc("Syscall table path"),
                                             cl::value_desc("table"));

static cl::list<std::string> AllowedABIs(cl::cat(LSWToolCategory), "arch",
                                         cl::Required, cl::CommaSeparated,
                                         cl::OneOrMore,
                                         cl::desc("<abi0> <abi1> ... <abiN>"));

static cl::list<std::string>
    SkippedSyscalls(cl::cat(LSWToolCategory), "skipped", cl::CommaSeparated,
                    cl::ZeroOrMore,
                    cl::desc("<syscall0> <syscall1> ... <syscallN>"));

static cl::list<std::string>
    ImplementationFiles(cl::cat(LSWToolCategory), cl::Required, cl::Positional,
                        cl::ZeroOrMore,
                        cl::desc("<file0> <file1> ... <fileN>"));

int main(int argc, const char **argv) {
  cl::HideUnrelatedOptions(LSWToolCategory);
  cl::ParseCommandLineOptions(argc, argv);

  // build the compilation db
  std::string CompilationDatabaseError;
  const std::unique_ptr<CompilationDatabase> CompilationDatabase =
      CompilationDatabase::autoDetectFromSource("./compile_commands.json",
                                                CompilationDatabaseError);
  if (CompilationDatabase == nullptr) {
    errs() << CompilationDatabaseError << "\n";
    return 1;
  }

  // find syscall entrypoints
  ClangTool FindEntrypoints(*CompilationDatabase, ImplementationFiles);
  FindEntrypoints.run(
      newFrontendActionFactory<lsw::FindEntrypoints::Action>().get());

  // parse the syscall table and build the full syscall info
  std::vector<lsw::Syscall> Syscalls;
  lsw::parseSyscallTable(SyscallTableFile, Syscalls, AllowedABIs,
                         SkippedSyscalls);

  // print the output
  lsw::outputJson(Syscalls, outs());

  return 0;
}
