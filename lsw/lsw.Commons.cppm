module;

#include "Precompiled.hpp"

export module lsw.Commons;

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

export namespace lsw {

struct FunctionParam {
  std::string Name;
  std::string Type;
  bool IsPointer;
};

struct Function {
  std::string Name;
  std::string ReturnType;
  std::vector<FunctionParam> Params;
};

struct Syscall {
  std::string Name;
  std::string ABI;
  Function *Entrypoint;
  int Number;
};

struct FunctionMapInfo {
  static inline Function getEmptyKey() { return {.Name = ""}; }

  static inline Function getTombstoneKey() {
    return {.Name = std::string("\0", 1)};
  }

  static unsigned getHashValue(const Function &Def) {
    return hash_value(Def.Name);
  }

  static unsigned getHashValue(const StringRef &Def) { return hash_value(Def); }

  static bool isEqual(const Function &LHS, const Function &RHS) {
    return hash_value(LHS.Name) == hash_value(RHS.Name);
  }

  static bool isEqual(const StringRef &LHS, const Function &RHS) {
    return hash_value(LHS) == hash_value(RHS.Name);
  }
};

// This is a global since:
// - Clang has a quite convolute way of instancing an AST consumer
// - subclassing might incur into API breakage
// - this set is needed everywhere
DenseSet<lsw::Function, lsw::FunctionMapInfo> Entrypoints;

} // namespace lsw
