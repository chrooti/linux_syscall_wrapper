module;

#include "Precompiled.hpp"

export module lsw.ParseTable;

import lsw.Commons;

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

export namespace lsw {

int parseSyscallTable(Twine Path, std::vector<Syscall> &Result,
                      std::vector<std::string> &AllowedABIs,
                      std::vector<std::string> &SkippedSyscalls) {
  ErrorOr<std::unique_ptr<MemoryBuffer>> File =
      MemoryBuffer::getFile(Path, true, false);
  if (!File || !*File) {
    return -1;
  }

  for (line_iterator It = line_iterator(MemoryBufferRef(**File));
       !It.is_at_end(); It++) {

    // skip comments
    if (It->startswith("#")) {
      continue;
    }

    // skip empty lines
    if (It->size() == 0) {
      continue;
    }

    size_t FirstSpace = It->find_first_of('\t', 0);
    const StringRef Number = It->substr(0, FirstSpace);

    size_t FirstNonSpace = It->find_first_not_of('\t', FirstSpace);
    FirstSpace = It->find_first_of('\t', FirstNonSpace);
    const StringRef ABI = It->substr(FirstNonSpace, FirstSpace - FirstNonSpace);
    if (std::find(AllowedABIs.begin(), AllowedABIs.end(), ABI) ==
        AllowedABIs.end()) {
      // not in the ABI
      continue;
    }

    FirstNonSpace = It->find_first_not_of('\t', FirstSpace);
    FirstSpace = It->find_first_of('\t', FirstNonSpace);
    const StringRef Name =
        It->substr(FirstNonSpace, FirstSpace - FirstNonSpace);
    if (std::find(SkippedSyscalls.begin(), SkippedSyscalls.end(), Name) !=
        SkippedSyscalls.end()) {
      // should be omitted for user request
      continue;
    }

    FirstNonSpace = It->find_first_not_of('\t', FirstSpace);
    FirstSpace = It->find_first_of('\t', FirstNonSpace);
    const StringRef EntrypointName =
        It->substr(FirstNonSpace, FirstSpace - FirstNonSpace);

    if (EntrypointName.size() == 0) {
      // no entrypoint
      continue;
    }

    auto Entrypoint = Entrypoints.find_as(EntrypointName);
    if (Entrypoint == Entrypoints.end()) {
      errs() << "[WARNING]: " << EntrypointName
             << " was not found in the declarations\n";
      continue;
    }

    Result.emplace_back(::lsw::Syscall{
        .Name = Name.str(),
        .ABI = ABI.str(),

        // safe: Entrypoint is stored in a container with global scope
        .Entrypoint = &*Entrypoint,

        .Number = std::stoi(Number.str()),
    });
  }

  return 0;
}

} // namespace lsw
