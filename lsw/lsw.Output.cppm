module;

#include "Precompiled.hpp"

export module lsw.Output;

import lsw.Commons;

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

export namespace lsw {

void outputJson(std::vector<lsw::Syscall> &Syscalls, raw_ostream &OS) {
  json::OStream Json{OS};

  Json.object([&] {
    for (const Syscall &Syscall : Syscalls) {
      Json.attributeObject(Syscall.Name, [&] {
        Json.attribute("ABI", Syscall.ABI);

        Json.attributeObject("entrypoint", [&] {
          const Function *Entrypoint = Syscall.Entrypoint;
          Json.attribute("name", Entrypoint->Name);
          Json.attribute("returnType", Entrypoint->ReturnType);
          Json.attributeArray("params", [&] {
            for (const FunctionParam &Param : Entrypoint->Params) {
              Json.object([&] {
                Json.attribute("name", Param.Name);
                Json.attribute("type", Param.Type);
                Json.attribute("isPointer", Param.IsPointer);
              });
            }
          });
        });

        Json.attribute("number", Syscall.Number);
      });
    }
  });

  Json.flush();
}

} // namespace lsw
