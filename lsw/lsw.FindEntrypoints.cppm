module;

#include "Precompiled.hpp"

export module lsw.FindEntrypoints;

import lsw.Commons;

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

export namespace lsw::FindEntrypoints {

class ASTVisitor : public RecursiveASTVisitor<ASTVisitor> {
public:
  bool VisitFunctionDecl(FunctionDecl *D);

private:
  // can't deduce default template arg
  static const StringSet<MallocAllocator> UnwantedAliases;
};

class ASTConsumer : public ::clang::ASTConsumer {
public:
  void HandleTranslationUnit(ASTContext &Context) override;

private:
  ASTVisitor Visitor;
};

class Action : public ASTFrontendAction {
public:
  std::unique_ptr<::clang::ASTConsumer>
  CreateASTConsumer(CompilerInstance &CI, StringRef InFile) override;
};

const StringSet ASTVisitor::UnwantedAliases{
    "__u32",
    "u32",
    "__s32",
    "s32",
};

bool ASTVisitor::VisitFunctionDecl(FunctionDecl *D) {
  // skip if the funciton is not the first declaration, we need only the
  // signature
  if (!D->isFirstDecl()) {
    return true;
  }

  // skip if it's not a syscall definition
  const StringRef EntrypointName = D->getName();
  if (!EntrypointName.startswith("__do_sys_") &&
      !EntrypointName.startswith("__do_compat_sys")) {
    return true;
  }

  // output the parameters in the declaration
  std::vector<FunctionParam> Params;
  for (ParmVarDecl *Param : D->parameters()) {

    QualType ParamType = Param->getType();

    QualType BaseType = ParamType;
    while (BaseType->isPointerType()) {
      BaseType = BaseType->getPointeeType();
    }
    QualType BaseUnqualType = BaseType.getUnqualifiedType();

    // substitute unwanted aliases with the canonical type
    // e.g. replace u32 and __u32 with uint32_t
    if (UnwantedAliases.contains(BaseUnqualType.getAsString())) {
      BaseUnqualType = BaseUnqualType.getCanonicalType();
      ParamType = QualType(BaseUnqualType.getTypePtr(),
                           ParamType.getQualifiers().getAsOpaqueValue());
    }

    Params.emplace_back(FunctionParam{
        Param->getName().str(),
        ParamType.getAsString(),
        ParamType->isPointerType(),
    });
  }

  // every macro defines a syscall on a function named sys_X
  const size_t FunctionNamePrefixLength = sizeof("__do_") - 1;

  Entrypoints.insert(Function{
      .Name =
          EntrypointName.slice(FunctionNamePrefixLength, StringRef::npos).str(),
      .ReturnType = D->getReturnType().getAsString(),
      .Params = std::move(Params),
  });

  return true;
}

void ASTConsumer::HandleTranslationUnit(ASTContext &Context) {
  Visitor.TraverseDecl(Context.getTranslationUnitDecl());
}

std::unique_ptr<::clang::ASTConsumer>
Action::CreateASTConsumer(CompilerInstance &CI, StringRef InFile) {
  (void)InFile;

  CI.getFrontendOpts().SkipFunctionBodies = true;
  CI.getDiagnostics().setClient(new IgnoringDiagConsumer());

  return std::make_unique<ASTConsumer>();
}

} // namespace lsw::FindEntrypoints
