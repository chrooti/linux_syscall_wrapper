#ifndef LSW_PRECOMPILED_H
#define LSW_PRECOMPILED_H

// hack: since llvm is built with c++17 on some distro
// and libstdc++ defines different implementation of things with and
// without context this ends up causing linking issue
// so we disable concepts by faking the header as already included
#define _GLIBCXX_CONCEPTS 1

#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Tooling/CompilationDatabase.h>
#include <clang/Tooling/Tooling.h>
#include <llvm/Support/JSON.h>
#include <llvm/Support/LineIterator.h>

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

#endif
