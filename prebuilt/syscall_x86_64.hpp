#ifndef LINUX_SYSCALL_WRAPPER
#define LINUX_SYSCALL_WRAPPER

#include <stddef.h>
#include <stdint.h>

#if defined(__clang__) || defined(__GNUC__)
#define LSW_INLINE static inline __attribute__((__always_inline__, __unused__))
#define LSW_UNUSED __attribute__((__unused__))
#else
#define LSW_INLINE static inline
#define LSW_UNUSED
#endif

#include <linux/version.h>

#include <linux/aio_abi.h>
#include <linux/auxvec.h>
#include <linux/capability.h>
#include <linux/elf.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 13, 0)
#include <linux/landlock.h>
#endif
#include <linux/posix_types.h>
#include <linux/signal.h>
#include <linux/types.h>
#include <linux/unistd.h>

using umode_t = unsigned short;
using key_serial_t = int32_t;
using qid_t = __kernel_uid32_t;

using clockid_t = __kernel_clockid_t;
using fd_set = __kernel_fd_set;
using gid_t = __kernel_gid_t;
using key_t = __kernel_key_t;
using loff_t = __kernel_loff_t;
using mqd_t = __kernel_mqd_t;
using off_t = __kernel_off_t;
using pid_t = __kernel_pid_t;
using rwf_t = __kernel_rwf_t;
using timer_t = __kernel_timer_t;
using uid_t = __kernel_uid_t;

#if INTPTR_MAX == INT64_MAX
using ssize_t = int64_t;
#elif INTPTR_MAX == INT32_MAX
using ssize_t = int32_t;
#else
#error "Cannot detect pointer size"
#endif

namespace syscall {

namespace detail {

/* UTILS */

bool strncmp(const char* s1, const char *s2, size_t s2_len) {
    for(size_t i = 0; i < s2_len; i++) {
        if(s1[i] == '\0') {
            return false;
        }

        if(s1[i] != s2[i]) {
            return false;
        }
    }

    return true;
}

/* GENERIC SYSCALLS */

LSW_INLINE ssize_t syscall0(
    ssize_t nr
) {
    ssize_t ret;

    asm volatile("syscall"

                // rax
                : "=a"(ret)

                // rax, rdi, rsi, rdx, r10, r8, r9
                :
                "a" (nr)

                :
                "rcx",
                "r11",
                "memory"
    );

    return ret;
}

LSW_INLINE ssize_t syscall1(
    ssize_t nr
    ,
    ssize_t arg1
) {
    ssize_t ret;

    asm volatile("syscall"

                // rax
                : "=a"(ret)

                // rax, rdi, rsi, rdx, r10, r8, r9
                :
                "a" (nr)
                ,
                    "D" (arg1)

                :
                "rcx",
                "r11",
                "memory"
    );

    return ret;
}

LSW_INLINE ssize_t syscall2(
    ssize_t nr
    ,
    ssize_t arg1,
    ssize_t arg2
) {
    ssize_t ret;

    asm volatile("syscall"

                // rax
                : "=a"(ret)

                // rax, rdi, rsi, rdx, r10, r8, r9
                :
                "a" (nr)
                ,
                    "D" (arg1),
                    "S" (arg2)

                :
                "rcx",
                "r11",
                "memory"
    );

    return ret;
}

LSW_INLINE ssize_t syscall3(
    ssize_t nr
    ,
    ssize_t arg1,
    ssize_t arg2,
    ssize_t arg3
) {
    ssize_t ret;

    asm volatile("syscall"

                // rax
                : "=a"(ret)

                // rax, rdi, rsi, rdx, r10, r8, r9
                :
                "a" (nr)
                ,
                    "D" (arg1),
                    "S" (arg2),
                    "d" (arg3)

                :
                "rcx",
                "r11",
                "memory"
    );

    return ret;
}

LSW_INLINE ssize_t syscall4(
    ssize_t nr
    ,
    ssize_t arg1,
    ssize_t arg2,
    ssize_t arg3,
    ssize_t arg4
) {
    ssize_t ret;
    register ssize_t r10 asm("r10") = arg4;

    asm volatile("syscall"

                // rax
                : "=a"(ret)

                // rax, rdi, rsi, rdx, r10, r8, r9
                :
                "a" (nr)
                ,
                    "D" (arg1),
                    "S" (arg2),
                    "d" (arg3),
                    "r" (r10)

                :
                "rcx",
                "r11",
                "memory"
    );

    return ret;
}

LSW_INLINE ssize_t syscall5(
    ssize_t nr
    ,
    ssize_t arg1,
    ssize_t arg2,
    ssize_t arg3,
    ssize_t arg4,
    ssize_t arg5
) {
    ssize_t ret;
    register ssize_t r10 asm("r10") = arg4;
    register ssize_t r8 asm("r8") = arg5;

    asm volatile("syscall"

                // rax
                : "=a"(ret)

                // rax, rdi, rsi, rdx, r10, r8, r9
                :
                "a" (nr)
                ,
                    "D" (arg1),
                    "S" (arg2),
                    "d" (arg3),
                    "r" (r10),
                    "r" (r8)

                :
                "rcx",
                "r11",
                "memory"
    );

    return ret;
}

LSW_INLINE ssize_t syscall6(
    ssize_t nr
    ,
    ssize_t arg1,
    ssize_t arg2,
    ssize_t arg3,
    ssize_t arg4,
    ssize_t arg5,
    ssize_t arg6
) {
    ssize_t ret;
    register ssize_t r10 asm("r10") = arg4;
    register ssize_t r8 asm("r8") = arg5;
    register ssize_t r9 asm("r9") = arg6;

    asm volatile("syscall"

                // rax
                : "=a"(ret)

                // rax, rdi, rsi, rdx, r10, r8, r9
                :
                "a" (nr)
                ,
                    "D" (arg1),
                    "S" (arg2),
                    "d" (arg3),
                    "r" (r10),
                    "r" (r8),
                    "r" (r9)

                :
                "rcx",
                "r11",
                "memory"
    );

    return ret;
}

} // namespace detail

#ifdef __NR_read
LSW_UNUSED ssize_t read(
    unsigned int fd,
    char * buf,
    size_t count
) {
  return ::syscall::detail::syscall3(
    __NR_read,
    fd,
    reinterpret_cast<ssize_t>(buf),
    count
  );
}
#endif

#ifdef __NR_write
LSW_UNUSED ssize_t write(
    unsigned int fd,
    const char * buf,
    size_t count
) {
  return ::syscall::detail::syscall3(
    __NR_write,
    fd,
    reinterpret_cast<ssize_t>(buf),
    count
  );
}
#endif

#ifdef __NR_open
LSW_UNUSED ssize_t open(
    const char * filename,
    int flags,
    umode_t mode
) {
  return ::syscall::detail::syscall3(
    __NR_open,
    reinterpret_cast<ssize_t>(filename),
    flags,
    mode
  );
}
#endif

#ifdef __NR_close
LSW_UNUSED ssize_t close(
    unsigned int fd
) {
  return ::syscall::detail::syscall1(
    __NR_close,
    fd
  );
}
#endif

#ifdef __NR_stat
LSW_UNUSED ssize_t stat(
    const char * filename,
    struct stat * statbuf
) {
  return ::syscall::detail::syscall2(
    __NR_stat,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(statbuf)
  );
}
#endif

#ifdef __NR_fstat
LSW_UNUSED ssize_t fstat(
    unsigned int fd,
    struct stat * statbuf
) {
  return ::syscall::detail::syscall2(
    __NR_fstat,
    fd,
    reinterpret_cast<ssize_t>(statbuf)
  );
}
#endif

#ifdef __NR_lstat
LSW_UNUSED ssize_t lstat(
    const char * filename,
    struct stat * statbuf
) {
  return ::syscall::detail::syscall2(
    __NR_lstat,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(statbuf)
  );
}
#endif

#ifdef __NR_poll
LSW_UNUSED ssize_t poll(
    struct pollfd * ufds,
    unsigned int nfds,
    int timeout_msecs
) {
  return ::syscall::detail::syscall3(
    __NR_poll,
    reinterpret_cast<ssize_t>(ufds),
    nfds,
    timeout_msecs
  );
}
#endif

#ifdef __NR_lseek
LSW_UNUSED ssize_t lseek(
    unsigned int fd,
    off_t offset,
    unsigned int whence
) {
  return ::syscall::detail::syscall3(
    __NR_lseek,
    fd,
    offset,
    whence
  );
}
#endif

#ifdef __NR_mmap
LSW_UNUSED ssize_t mmap(
    unsigned long addr,
    unsigned long len,
    unsigned long prot,
    unsigned long flags,
    unsigned long fd,
    unsigned long off
) {
  return ::syscall::detail::syscall6(
    __NR_mmap,
    addr,
    len,
    prot,
    flags,
    fd,
    off
  );
}
#endif

#ifdef __NR_mprotect
LSW_UNUSED ssize_t mprotect(
    unsigned long start,
    size_t len,
    unsigned long prot
) {
  return ::syscall::detail::syscall3(
    __NR_mprotect,
    start,
    len,
    prot
  );
}
#endif

#ifdef __NR_munmap
LSW_UNUSED ssize_t munmap(
    unsigned long addr,
    size_t len
) {
  return ::syscall::detail::syscall2(
    __NR_munmap,
    addr,
    len
  );
}
#endif

#ifdef __NR_brk
LSW_UNUSED ssize_t brk(
    unsigned long brk
) {
  return ::syscall::detail::syscall1(
    __NR_brk,
    brk
  );
}
#endif

#ifdef __NR_rt_sigaction
LSW_UNUSED ssize_t rt_sigaction(
    int sig,
    const struct sigaction * act,
    struct sigaction * oact,
    size_t sigsetsize
) {
  return ::syscall::detail::syscall4(
    __NR_rt_sigaction,
    sig,
    reinterpret_cast<ssize_t>(act),
    reinterpret_cast<ssize_t>(oact),
    sigsetsize
  );
}
#endif

#ifdef __NR_rt_sigprocmask
LSW_UNUSED ssize_t rt_sigprocmask(
    int how,
    sigset_t * nset,
    sigset_t * oset,
    size_t sigsetsize
) {
  return ::syscall::detail::syscall4(
    __NR_rt_sigprocmask,
    how,
    reinterpret_cast<ssize_t>(nset),
    reinterpret_cast<ssize_t>(oset),
    sigsetsize
  );
}
#endif

#ifdef __NR_rt_sigreturn
LSW_UNUSED ssize_t rt_sigreturn(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_rt_sigreturn,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_ioctl
LSW_UNUSED ssize_t ioctl(
    unsigned int fd,
    unsigned int cmd,
    unsigned long arg
) {
  return ::syscall::detail::syscall3(
    __NR_ioctl,
    fd,
    cmd,
    arg
  );
}
#endif

#ifdef __NR_pread64
LSW_UNUSED ssize_t pread64(
    unsigned int fd,
    char * buf,
    size_t count,
    loff_t pos
) {
  return ::syscall::detail::syscall4(
    __NR_pread64,
    fd,
    reinterpret_cast<ssize_t>(buf),
    count,
    pos
  );
}
#endif

#ifdef __NR_pwrite64
LSW_UNUSED ssize_t pwrite64(
    unsigned int fd,
    const char * buf,
    size_t count,
    loff_t pos
) {
  return ::syscall::detail::syscall4(
    __NR_pwrite64,
    fd,
    reinterpret_cast<ssize_t>(buf),
    count,
    pos
  );
}
#endif

#ifdef __NR_readv
LSW_UNUSED ssize_t readv(
    unsigned long fd,
    const struct iovec * vec,
    unsigned long vlen
) {
  return ::syscall::detail::syscall3(
    __NR_readv,
    fd,
    reinterpret_cast<ssize_t>(vec),
    vlen
  );
}
#endif

#ifdef __NR_writev
LSW_UNUSED ssize_t writev(
    unsigned long fd,
    const struct iovec * vec,
    unsigned long vlen
) {
  return ::syscall::detail::syscall3(
    __NR_writev,
    fd,
    reinterpret_cast<ssize_t>(vec),
    vlen
  );
}
#endif

#ifdef __NR_access
LSW_UNUSED ssize_t access(
    const char * filename,
    int mode
) {
  return ::syscall::detail::syscall2(
    __NR_access,
    reinterpret_cast<ssize_t>(filename),
    mode
  );
}
#endif

#ifdef __NR_pipe
LSW_UNUSED ssize_t pipe(
    int * fildes
) {
  return ::syscall::detail::syscall1(
    __NR_pipe,
    reinterpret_cast<ssize_t>(fildes)
  );
}
#endif

#ifdef __NR_select
LSW_UNUSED ssize_t select(
    int n,
    fd_set * inp,
    fd_set * outp,
    fd_set * exp,
    struct __kernel_old_timeval * tvp
) {
  return ::syscall::detail::syscall5(
    __NR_select,
    n,
    reinterpret_cast<ssize_t>(inp),
    reinterpret_cast<ssize_t>(outp),
    reinterpret_cast<ssize_t>(exp),
    reinterpret_cast<ssize_t>(tvp)
  );
}
#endif

#ifdef __NR_sched_yield
LSW_UNUSED ssize_t sched_yield(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_sched_yield,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_mremap
LSW_UNUSED ssize_t mremap(
    unsigned long addr,
    unsigned long old_len,
    unsigned long new_len,
    unsigned long flags,
    unsigned long new_addr
) {
  return ::syscall::detail::syscall5(
    __NR_mremap,
    addr,
    old_len,
    new_len,
    flags,
    new_addr
  );
}
#endif

#ifdef __NR_msync
LSW_UNUSED ssize_t msync(
    unsigned long start,
    size_t len,
    int flags
) {
  return ::syscall::detail::syscall3(
    __NR_msync,
    start,
    len,
    flags
  );
}
#endif

#ifdef __NR_mincore
LSW_UNUSED ssize_t mincore(
    unsigned long start,
    size_t len,
    unsigned char * vec
) {
  return ::syscall::detail::syscall3(
    __NR_mincore,
    start,
    len,
    reinterpret_cast<ssize_t>(vec)
  );
}
#endif

#ifdef __NR_madvise
LSW_UNUSED ssize_t madvise(
    unsigned long start,
    size_t len_in,
    int behavior
) {
  return ::syscall::detail::syscall3(
    __NR_madvise,
    start,
    len_in,
    behavior
  );
}
#endif

#ifdef __NR_shmget
LSW_UNUSED ssize_t shmget(
    key_t key,
    size_t size,
    int shmflg
) {
  return ::syscall::detail::syscall3(
    __NR_shmget,
    key,
    size,
    shmflg
  );
}
#endif

#ifdef __NR_shmat
LSW_UNUSED ssize_t shmat(
    int shmid,
    char * shmaddr,
    int shmflg
) {
  return ::syscall::detail::syscall3(
    __NR_shmat,
    shmid,
    reinterpret_cast<ssize_t>(shmaddr),
    shmflg
  );
}
#endif

#ifdef __NR_shmctl
LSW_UNUSED ssize_t shmctl(
    int shmid,
    int cmd,
    struct shmid_ds * buf
) {
  return ::syscall::detail::syscall3(
    __NR_shmctl,
    shmid,
    cmd,
    reinterpret_cast<ssize_t>(buf)
  );
}
#endif

#ifdef __NR_dup
LSW_UNUSED ssize_t dup(
    unsigned int fildes
) {
  return ::syscall::detail::syscall1(
    __NR_dup,
    fildes
  );
}
#endif

#ifdef __NR_dup2
LSW_UNUSED ssize_t dup2(
    unsigned int oldfd,
    unsigned int newfd
) {
  return ::syscall::detail::syscall2(
    __NR_dup2,
    oldfd,
    newfd
  );
}
#endif

#ifdef __NR_pause
LSW_UNUSED ssize_t pause(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_pause,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_nanosleep
LSW_UNUSED ssize_t nanosleep(
    struct __kernel_timespec * rqtp,
    struct __kernel_timespec * rmtp
) {
  return ::syscall::detail::syscall2(
    __NR_nanosleep,
    reinterpret_cast<ssize_t>(rqtp),
    reinterpret_cast<ssize_t>(rmtp)
  );
}
#endif

#ifdef __NR_getitimer
LSW_UNUSED ssize_t getitimer(
    int which,
    struct __kernel_old_itimerval * value
) {
  return ::syscall::detail::syscall2(
    __NR_getitimer,
    which,
    reinterpret_cast<ssize_t>(value)
  );
}
#endif

#ifdef __NR_alarm
LSW_UNUSED ssize_t alarm(
    unsigned int seconds
) {
  return ::syscall::detail::syscall1(
    __NR_alarm,
    seconds
  );
}
#endif

#ifdef __NR_setitimer
LSW_UNUSED ssize_t setitimer(
    int which,
    struct __kernel_old_itimerval * value,
    struct __kernel_old_itimerval * ovalue
) {
  return ::syscall::detail::syscall3(
    __NR_setitimer,
    which,
    reinterpret_cast<ssize_t>(value),
    reinterpret_cast<ssize_t>(ovalue)
  );
}
#endif

#ifdef __NR_getpid
LSW_UNUSED ssize_t getpid(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_getpid,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_sendfile
LSW_UNUSED ssize_t sendfile(
    int out_fd,
    int in_fd,
    loff_t * offset,
    size_t count
) {
  return ::syscall::detail::syscall4(
    __NR_sendfile,
    out_fd,
    in_fd,
    reinterpret_cast<ssize_t>(offset),
    count
  );
}
#endif

#ifdef __NR_socket
LSW_UNUSED ssize_t socket(
    int family,
    int type,
    int protocol
) {
  return ::syscall::detail::syscall3(
    __NR_socket,
    family,
    type,
    protocol
  );
}
#endif

#ifdef __NR_connect
LSW_UNUSED ssize_t connect(
    int fd,
    struct sockaddr * uservaddr,
    int addrlen
) {
  return ::syscall::detail::syscall3(
    __NR_connect,
    fd,
    reinterpret_cast<ssize_t>(uservaddr),
    addrlen
  );
}
#endif

#ifdef __NR_accept
LSW_UNUSED ssize_t accept(
    int fd,
    struct sockaddr * upeer_sockaddr,
    int * upeer_addrlen
) {
  return ::syscall::detail::syscall3(
    __NR_accept,
    fd,
    reinterpret_cast<ssize_t>(upeer_sockaddr),
    reinterpret_cast<ssize_t>(upeer_addrlen)
  );
}
#endif

#ifdef __NR_sendto
LSW_UNUSED ssize_t sendto(
    int fd,
    void * buff,
    size_t len,
    unsigned int flags,
    struct sockaddr * addr,
    int addr_len
) {
  return ::syscall::detail::syscall6(
    __NR_sendto,
    fd,
    reinterpret_cast<ssize_t>(buff),
    len,
    flags,
    reinterpret_cast<ssize_t>(addr),
    addr_len
  );
}
#endif

#ifdef __NR_recvfrom
LSW_UNUSED ssize_t recvfrom(
    int fd,
    void * ubuf,
    size_t size,
    unsigned int flags,
    struct sockaddr * addr,
    int * addr_len
) {
  return ::syscall::detail::syscall6(
    __NR_recvfrom,
    fd,
    reinterpret_cast<ssize_t>(ubuf),
    size,
    flags,
    reinterpret_cast<ssize_t>(addr),
    reinterpret_cast<ssize_t>(addr_len)
  );
}
#endif

#ifdef __NR_sendmsg
LSW_UNUSED ssize_t sendmsg(
    int fd,
    struct user_msghdr * msg,
    unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_sendmsg,
    fd,
    reinterpret_cast<ssize_t>(msg),
    flags
  );
}
#endif

#ifdef __NR_recvmsg
LSW_UNUSED ssize_t recvmsg(
    int fd,
    struct user_msghdr * msg,
    unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_recvmsg,
    fd,
    reinterpret_cast<ssize_t>(msg),
    flags
  );
}
#endif

#ifdef __NR_shutdown
LSW_UNUSED ssize_t shutdown(
    int fd,
    int how
) {
  return ::syscall::detail::syscall2(
    __NR_shutdown,
    fd,
    how
  );
}
#endif

#ifdef __NR_bind
LSW_UNUSED ssize_t bind(
    int fd,
    struct sockaddr * umyaddr,
    int addrlen
) {
  return ::syscall::detail::syscall3(
    __NR_bind,
    fd,
    reinterpret_cast<ssize_t>(umyaddr),
    addrlen
  );
}
#endif

#ifdef __NR_listen
LSW_UNUSED ssize_t listen(
    int fd,
    int backlog
) {
  return ::syscall::detail::syscall2(
    __NR_listen,
    fd,
    backlog
  );
}
#endif

#ifdef __NR_getsockname
LSW_UNUSED ssize_t getsockname(
    int fd,
    struct sockaddr * usockaddr,
    int * usockaddr_len
) {
  return ::syscall::detail::syscall3(
    __NR_getsockname,
    fd,
    reinterpret_cast<ssize_t>(usockaddr),
    reinterpret_cast<ssize_t>(usockaddr_len)
  );
}
#endif

#ifdef __NR_getpeername
LSW_UNUSED ssize_t getpeername(
    int fd,
    struct sockaddr * usockaddr,
    int * usockaddr_len
) {
  return ::syscall::detail::syscall3(
    __NR_getpeername,
    fd,
    reinterpret_cast<ssize_t>(usockaddr),
    reinterpret_cast<ssize_t>(usockaddr_len)
  );
}
#endif

#ifdef __NR_socketpair
LSW_UNUSED ssize_t socketpair(
    int family,
    int type,
    int protocol,
    int * usockvec
) {
  return ::syscall::detail::syscall4(
    __NR_socketpair,
    family,
    type,
    protocol,
    reinterpret_cast<ssize_t>(usockvec)
  );
}
#endif

#ifdef __NR_setsockopt
LSW_UNUSED ssize_t setsockopt(
    int fd,
    int level,
    int optname,
    char * optval,
    int optlen
) {
  return ::syscall::detail::syscall5(
    __NR_setsockopt,
    fd,
    level,
    optname,
    reinterpret_cast<ssize_t>(optval),
    optlen
  );
}
#endif

#ifdef __NR_getsockopt
LSW_UNUSED ssize_t getsockopt(
    int fd,
    int level,
    int optname,
    char * optval,
    int * optlen
) {
  return ::syscall::detail::syscall5(
    __NR_getsockopt,
    fd,
    level,
    optname,
    reinterpret_cast<ssize_t>(optval),
    reinterpret_cast<ssize_t>(optlen)
  );
}
#endif

#ifdef __NR_execve
LSW_UNUSED ssize_t execve(
    const char * filename,
    const char *const * argv,
    const char *const * envp
) {
  return ::syscall::detail::syscall3(
    __NR_execve,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(argv),
    reinterpret_cast<ssize_t>(envp)
  );
}
#endif

#ifdef __NR_exit
LSW_UNUSED ssize_t exit(
    int error_code
) {
  return ::syscall::detail::syscall1(
    __NR_exit,
    error_code
  );
}
#endif

#ifdef __NR_wait4
LSW_UNUSED ssize_t wait4(
    pid_t upid,
    int * stat_addr,
    int options,
    struct rusage * ru
) {
  return ::syscall::detail::syscall4(
    __NR_wait4,
    upid,
    reinterpret_cast<ssize_t>(stat_addr),
    options,
    reinterpret_cast<ssize_t>(ru)
  );
}
#endif

#ifdef __NR_kill
LSW_UNUSED ssize_t kill(
    pid_t pid,
    int sig
) {
  return ::syscall::detail::syscall2(
    __NR_kill,
    pid,
    sig
  );
}
#endif

#ifdef __NR_uname
LSW_UNUSED ssize_t uname(
    struct new_utsname * name
) {
  return ::syscall::detail::syscall1(
    __NR_uname,
    reinterpret_cast<ssize_t>(name)
  );
}
#endif

#ifdef __NR_semget
LSW_UNUSED ssize_t semget(
    key_t key,
    int nsems,
    int semflg
) {
  return ::syscall::detail::syscall3(
    __NR_semget,
    key,
    nsems,
    semflg
  );
}
#endif

#ifdef __NR_semop
LSW_UNUSED ssize_t semop(
    int semid,
    struct sembuf * tsops,
    unsigned int nsops
) {
  return ::syscall::detail::syscall3(
    __NR_semop,
    semid,
    reinterpret_cast<ssize_t>(tsops),
    nsops
  );
}
#endif

#ifdef __NR_semctl
LSW_UNUSED ssize_t semctl(
    int semid,
    int semnum,
    int cmd,
    unsigned long arg
) {
  return ::syscall::detail::syscall4(
    __NR_semctl,
    semid,
    semnum,
    cmd,
    arg
  );
}
#endif

#ifdef __NR_shmdt
LSW_UNUSED ssize_t shmdt(
    char * shmaddr
) {
  return ::syscall::detail::syscall1(
    __NR_shmdt,
    reinterpret_cast<ssize_t>(shmaddr)
  );
}
#endif

#ifdef __NR_msgget
LSW_UNUSED ssize_t msgget(
    key_t key,
    int msgflg
) {
  return ::syscall::detail::syscall2(
    __NR_msgget,
    key,
    msgflg
  );
}
#endif

#ifdef __NR_msgsnd
LSW_UNUSED ssize_t msgsnd(
    int msqid,
    struct msgbuf * msgp,
    size_t msgsz,
    int msgflg
) {
  return ::syscall::detail::syscall4(
    __NR_msgsnd,
    msqid,
    reinterpret_cast<ssize_t>(msgp),
    msgsz,
    msgflg
  );
}
#endif

#ifdef __NR_msgrcv
LSW_UNUSED ssize_t msgrcv(
    int msqid,
    struct msgbuf * msgp,
    size_t msgsz,
    long msgtyp,
    int msgflg
) {
  return ::syscall::detail::syscall5(
    __NR_msgrcv,
    msqid,
    reinterpret_cast<ssize_t>(msgp),
    msgsz,
    msgtyp,
    msgflg
  );
}
#endif

#ifdef __NR_msgctl
LSW_UNUSED ssize_t msgctl(
    int msqid,
    int cmd,
    struct msqid_ds * buf
) {
  return ::syscall::detail::syscall3(
    __NR_msgctl,
    msqid,
    cmd,
    reinterpret_cast<ssize_t>(buf)
  );
}
#endif

#ifdef __NR_fcntl
LSW_UNUSED ssize_t fcntl(
    unsigned int fd,
    unsigned int cmd,
    unsigned long arg
) {
  return ::syscall::detail::syscall3(
    __NR_fcntl,
    fd,
    cmd,
    arg
  );
}
#endif

#ifdef __NR_flock
LSW_UNUSED ssize_t flock(
    unsigned int fd,
    unsigned int cmd
) {
  return ::syscall::detail::syscall2(
    __NR_flock,
    fd,
    cmd
  );
}
#endif

#ifdef __NR_fsync
LSW_UNUSED ssize_t fsync(
    unsigned int fd
) {
  return ::syscall::detail::syscall1(
    __NR_fsync,
    fd
  );
}
#endif

#ifdef __NR_fdatasync
LSW_UNUSED ssize_t fdatasync(
    unsigned int fd
) {
  return ::syscall::detail::syscall1(
    __NR_fdatasync,
    fd
  );
}
#endif

#ifdef __NR_truncate
LSW_UNUSED ssize_t truncate(
    const char * path,
    long length
) {
  return ::syscall::detail::syscall2(
    __NR_truncate,
    reinterpret_cast<ssize_t>(path),
    length
  );
}
#endif

#ifdef __NR_ftruncate
LSW_UNUSED ssize_t ftruncate(
    unsigned int fd,
    unsigned long length
) {
  return ::syscall::detail::syscall2(
    __NR_ftruncate,
    fd,
    length
  );
}
#endif

#ifdef __NR_getdents
LSW_UNUSED ssize_t getdents(
    unsigned int fd,
    struct linux_dirent * dirent,
    unsigned int count
) {
  return ::syscall::detail::syscall3(
    __NR_getdents,
    fd,
    reinterpret_cast<ssize_t>(dirent),
    count
  );
}
#endif

#ifdef __NR_getcwd
LSW_UNUSED ssize_t getcwd(
    char * buf,
    unsigned long size
) {
  return ::syscall::detail::syscall2(
    __NR_getcwd,
    reinterpret_cast<ssize_t>(buf),
    size
  );
}
#endif

#ifdef __NR_chdir
LSW_UNUSED ssize_t chdir(
    const char * filename
) {
  return ::syscall::detail::syscall1(
    __NR_chdir,
    reinterpret_cast<ssize_t>(filename)
  );
}
#endif

#ifdef __NR_fchdir
LSW_UNUSED ssize_t fchdir(
    unsigned int fd
) {
  return ::syscall::detail::syscall1(
    __NR_fchdir,
    fd
  );
}
#endif

#ifdef __NR_rename
LSW_UNUSED ssize_t rename(
    const char * oldname,
    const char * newname
) {
  return ::syscall::detail::syscall2(
    __NR_rename,
    reinterpret_cast<ssize_t>(oldname),
    reinterpret_cast<ssize_t>(newname)
  );
}
#endif

#ifdef __NR_mkdir
LSW_UNUSED ssize_t mkdir(
    const char * pathname,
    umode_t mode
) {
  return ::syscall::detail::syscall2(
    __NR_mkdir,
    reinterpret_cast<ssize_t>(pathname),
    mode
  );
}
#endif

#ifdef __NR_rmdir
LSW_UNUSED ssize_t rmdir(
    const char * pathname
) {
  return ::syscall::detail::syscall1(
    __NR_rmdir,
    reinterpret_cast<ssize_t>(pathname)
  );
}
#endif

#ifdef __NR_creat
LSW_UNUSED ssize_t creat(
    const char * pathname,
    umode_t mode
) {
  return ::syscall::detail::syscall2(
    __NR_creat,
    reinterpret_cast<ssize_t>(pathname),
    mode
  );
}
#endif

#ifdef __NR_link
LSW_UNUSED ssize_t link(
    const char * oldname,
    const char * newname
) {
  return ::syscall::detail::syscall2(
    __NR_link,
    reinterpret_cast<ssize_t>(oldname),
    reinterpret_cast<ssize_t>(newname)
  );
}
#endif

#ifdef __NR_unlink
LSW_UNUSED ssize_t unlink(
    const char * pathname
) {
  return ::syscall::detail::syscall1(
    __NR_unlink,
    reinterpret_cast<ssize_t>(pathname)
  );
}
#endif

#ifdef __NR_symlink
LSW_UNUSED ssize_t symlink(
    const char * oldname,
    const char * newname
) {
  return ::syscall::detail::syscall2(
    __NR_symlink,
    reinterpret_cast<ssize_t>(oldname),
    reinterpret_cast<ssize_t>(newname)
  );
}
#endif

#ifdef __NR_readlink
LSW_UNUSED ssize_t readlink(
    const char * path,
    char * buf,
    int bufsiz
) {
  return ::syscall::detail::syscall3(
    __NR_readlink,
    reinterpret_cast<ssize_t>(path),
    reinterpret_cast<ssize_t>(buf),
    bufsiz
  );
}
#endif

#ifdef __NR_chmod
LSW_UNUSED ssize_t chmod(
    const char * filename,
    umode_t mode
) {
  return ::syscall::detail::syscall2(
    __NR_chmod,
    reinterpret_cast<ssize_t>(filename),
    mode
  );
}
#endif

#ifdef __NR_fchmod
LSW_UNUSED ssize_t fchmod(
    unsigned int fd,
    umode_t mode
) {
  return ::syscall::detail::syscall2(
    __NR_fchmod,
    fd,
    mode
  );
}
#endif

#ifdef __NR_chown
LSW_UNUSED ssize_t chown(
    const char * filename,
    uid_t user,
    gid_t group
) {
  return ::syscall::detail::syscall3(
    __NR_chown,
    reinterpret_cast<ssize_t>(filename),
    user,
    group
  );
}
#endif

#ifdef __NR_fchown
LSW_UNUSED ssize_t fchown(
    unsigned int fd,
    uid_t user,
    gid_t group
) {
  return ::syscall::detail::syscall3(
    __NR_fchown,
    fd,
    user,
    group
  );
}
#endif

#ifdef __NR_lchown
LSW_UNUSED ssize_t lchown(
    const char * filename,
    uid_t user,
    gid_t group
) {
  return ::syscall::detail::syscall3(
    __NR_lchown,
    reinterpret_cast<ssize_t>(filename),
    user,
    group
  );
}
#endif

#ifdef __NR_umask
LSW_UNUSED ssize_t umask(
    int mask
) {
  return ::syscall::detail::syscall1(
    __NR_umask,
    mask
  );
}
#endif

#ifdef __NR_getrlimit
LSW_UNUSED ssize_t getrlimit(
    unsigned int resource,
    struct rlimit * rlim
) {
  return ::syscall::detail::syscall2(
    __NR_getrlimit,
    resource,
    reinterpret_cast<ssize_t>(rlim)
  );
}
#endif

#ifdef __NR_getrusage
LSW_UNUSED ssize_t getrusage(
    int who,
    struct rusage * ru
) {
  return ::syscall::detail::syscall2(
    __NR_getrusage,
    who,
    reinterpret_cast<ssize_t>(ru)
  );
}
#endif

#ifdef __NR_sysinfo
LSW_UNUSED ssize_t sysinfo(
    struct sysinfo * info
) {
  return ::syscall::detail::syscall1(
    __NR_sysinfo,
    reinterpret_cast<ssize_t>(info)
  );
}
#endif

#ifdef __NR_times
LSW_UNUSED ssize_t times(
    struct tms * tbuf
) {
  return ::syscall::detail::syscall1(
    __NR_times,
    reinterpret_cast<ssize_t>(tbuf)
  );
}
#endif

#ifdef __NR_ptrace
LSW_UNUSED ssize_t ptrace(
    long request,
    long pid,
    unsigned long addr,
    unsigned long data
) {
  return ::syscall::detail::syscall4(
    __NR_ptrace,
    request,
    pid,
    addr,
    data
  );
}
#endif

#ifdef __NR_getuid
LSW_UNUSED ssize_t getuid(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_getuid,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_syslog
LSW_UNUSED ssize_t syslog(
    int type,
    char * buf,
    int len
) {
  return ::syscall::detail::syscall3(
    __NR_syslog,
    type,
    reinterpret_cast<ssize_t>(buf),
    len
  );
}
#endif

#ifdef __NR_getgid
LSW_UNUSED ssize_t getgid(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_getgid,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_setuid
LSW_UNUSED ssize_t setuid(
    uid_t uid
) {
  return ::syscall::detail::syscall1(
    __NR_setuid,
    uid
  );
}
#endif

#ifdef __NR_setgid
LSW_UNUSED ssize_t setgid(
    gid_t gid
) {
  return ::syscall::detail::syscall1(
    __NR_setgid,
    gid
  );
}
#endif

#ifdef __NR_geteuid
LSW_UNUSED ssize_t geteuid(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_geteuid,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_getegid
LSW_UNUSED ssize_t getegid(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_getegid,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_setpgid
LSW_UNUSED ssize_t setpgid(
    pid_t pid,
    pid_t pgid
) {
  return ::syscall::detail::syscall2(
    __NR_setpgid,
    pid,
    pgid
  );
}
#endif

#ifdef __NR_getppid
LSW_UNUSED ssize_t getppid(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_getppid,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_getpgrp
LSW_UNUSED ssize_t getpgrp(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_getpgrp,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_setsid
LSW_UNUSED ssize_t setsid(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_setsid,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_setreuid
LSW_UNUSED ssize_t setreuid(
    uid_t ruid,
    uid_t euid
) {
  return ::syscall::detail::syscall2(
    __NR_setreuid,
    ruid,
    euid
  );
}
#endif

#ifdef __NR_setregid
LSW_UNUSED ssize_t setregid(
    gid_t rgid,
    gid_t egid
) {
  return ::syscall::detail::syscall2(
    __NR_setregid,
    rgid,
    egid
  );
}
#endif

#ifdef __NR_getgroups
LSW_UNUSED ssize_t getgroups(
    int gidsetsize,
    gid_t * grouplist
) {
  return ::syscall::detail::syscall2(
    __NR_getgroups,
    gidsetsize,
    reinterpret_cast<ssize_t>(grouplist)
  );
}
#endif

#ifdef __NR_setgroups
LSW_UNUSED ssize_t setgroups(
    int gidsetsize,
    gid_t * grouplist
) {
  return ::syscall::detail::syscall2(
    __NR_setgroups,
    gidsetsize,
    reinterpret_cast<ssize_t>(grouplist)
  );
}
#endif

#ifdef __NR_setresuid
LSW_UNUSED ssize_t setresuid(
    uid_t ruid,
    uid_t euid,
    uid_t suid
) {
  return ::syscall::detail::syscall3(
    __NR_setresuid,
    ruid,
    euid,
    suid
  );
}
#endif

#ifdef __NR_getresuid
LSW_UNUSED ssize_t getresuid(
    uid_t * ruidp,
    uid_t * euidp,
    uid_t * suidp
) {
  return ::syscall::detail::syscall3(
    __NR_getresuid,
    reinterpret_cast<ssize_t>(ruidp),
    reinterpret_cast<ssize_t>(euidp),
    reinterpret_cast<ssize_t>(suidp)
  );
}
#endif

#ifdef __NR_setresgid
LSW_UNUSED ssize_t setresgid(
    gid_t rgid,
    gid_t egid,
    gid_t sgid
) {
  return ::syscall::detail::syscall3(
    __NR_setresgid,
    rgid,
    egid,
    sgid
  );
}
#endif

#ifdef __NR_getresgid
LSW_UNUSED ssize_t getresgid(
    gid_t * rgidp,
    gid_t * egidp,
    gid_t * sgidp
) {
  return ::syscall::detail::syscall3(
    __NR_getresgid,
    reinterpret_cast<ssize_t>(rgidp),
    reinterpret_cast<ssize_t>(egidp),
    reinterpret_cast<ssize_t>(sgidp)
  );
}
#endif

#ifdef __NR_getpgid
LSW_UNUSED ssize_t getpgid(
    pid_t pid
) {
  return ::syscall::detail::syscall1(
    __NR_getpgid,
    pid
  );
}
#endif

#ifdef __NR_setfsuid
LSW_UNUSED ssize_t setfsuid(
    uid_t uid
) {
  return ::syscall::detail::syscall1(
    __NR_setfsuid,
    uid
  );
}
#endif

#ifdef __NR_setfsgid
LSW_UNUSED ssize_t setfsgid(
    gid_t gid
) {
  return ::syscall::detail::syscall1(
    __NR_setfsgid,
    gid
  );
}
#endif

#ifdef __NR_getsid
LSW_UNUSED ssize_t getsid(
    pid_t pid
) {
  return ::syscall::detail::syscall1(
    __NR_getsid,
    pid
  );
}
#endif

#ifdef __NR_capget
LSW_UNUSED ssize_t capget(
    cap_user_header_t header,
    cap_user_data_t dataptr
) {
  return ::syscall::detail::syscall2(
    __NR_capget,
    reinterpret_cast<ssize_t>(header),
    reinterpret_cast<ssize_t>(dataptr)
  );
}
#endif

#ifdef __NR_capset
LSW_UNUSED ssize_t capset(
    cap_user_header_t header,
    const cap_user_data_t data
) {
  return ::syscall::detail::syscall2(
    __NR_capset,
    reinterpret_cast<ssize_t>(header),
    reinterpret_cast<ssize_t>(data)
  );
}
#endif

#ifdef __NR_rt_sigpending
LSW_UNUSED ssize_t rt_sigpending(
    sigset_t * uset,
    size_t sigsetsize
) {
  return ::syscall::detail::syscall2(
    __NR_rt_sigpending,
    reinterpret_cast<ssize_t>(uset),
    sigsetsize
  );
}
#endif

#ifdef __NR_rt_sigtimedwait
LSW_UNUSED ssize_t rt_sigtimedwait(
    const sigset_t * uthese,
    siginfo_t * uinfo,
    const struct __kernel_timespec * uts,
    size_t sigsetsize
) {
  return ::syscall::detail::syscall4(
    __NR_rt_sigtimedwait,
    reinterpret_cast<ssize_t>(uthese),
    reinterpret_cast<ssize_t>(uinfo),
    reinterpret_cast<ssize_t>(uts),
    sigsetsize
  );
}
#endif

#ifdef __NR_rt_sigqueueinfo
LSW_UNUSED ssize_t rt_sigqueueinfo(
    pid_t pid,
    int sig,
    siginfo_t * uinfo
) {
  return ::syscall::detail::syscall3(
    __NR_rt_sigqueueinfo,
    pid,
    sig,
    reinterpret_cast<ssize_t>(uinfo)
  );
}
#endif

#ifdef __NR_rt_sigsuspend
LSW_UNUSED ssize_t rt_sigsuspend(
    sigset_t * unewset,
    size_t sigsetsize
) {
  return ::syscall::detail::syscall2(
    __NR_rt_sigsuspend,
    reinterpret_cast<ssize_t>(unewset),
    sigsetsize
  );
}
#endif

#ifdef __NR_sigaltstack
LSW_UNUSED ssize_t sigaltstack(
    const stack_t * uss,
    stack_t * uoss
) {
  return ::syscall::detail::syscall2(
    __NR_sigaltstack,
    reinterpret_cast<ssize_t>(uss),
    reinterpret_cast<ssize_t>(uoss)
  );
}
#endif

#ifdef __NR_utime
LSW_UNUSED ssize_t utime(
    char * filename,
    struct utimbuf * times
) {
  return ::syscall::detail::syscall2(
    __NR_utime,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(times)
  );
}
#endif

#ifdef __NR_mknod
LSW_UNUSED ssize_t mknod(
    const char * filename,
    umode_t mode,
    unsigned int dev
) {
  return ::syscall::detail::syscall3(
    __NR_mknod,
    reinterpret_cast<ssize_t>(filename),
    mode,
    dev
  );
}
#endif

#ifdef __NR_personality
LSW_UNUSED ssize_t personality(
    unsigned int personality
) {
  return ::syscall::detail::syscall1(
    __NR_personality,
    personality
  );
}
#endif

#ifdef __NR_ustat
LSW_UNUSED ssize_t ustat(
    unsigned int dev,
    struct ustat * ubuf
) {
  return ::syscall::detail::syscall2(
    __NR_ustat,
    dev,
    reinterpret_cast<ssize_t>(ubuf)
  );
}
#endif

#ifdef __NR_statfs
LSW_UNUSED ssize_t statfs(
    const char * pathname,
    struct statfs * buf
) {
  return ::syscall::detail::syscall2(
    __NR_statfs,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(buf)
  );
}
#endif

#ifdef __NR_fstatfs
LSW_UNUSED ssize_t fstatfs(
    unsigned int fd,
    struct statfs * buf
) {
  return ::syscall::detail::syscall2(
    __NR_fstatfs,
    fd,
    reinterpret_cast<ssize_t>(buf)
  );
}
#endif

#ifdef __NR_sysfs
LSW_UNUSED ssize_t sysfs(
    int option,
    unsigned long arg1,
    unsigned long arg2
) {
  return ::syscall::detail::syscall3(
    __NR_sysfs,
    option,
    arg1,
    arg2
  );
}
#endif

#ifdef __NR_getpriority
LSW_UNUSED ssize_t getpriority(
    int which,
    int who
) {
  return ::syscall::detail::syscall2(
    __NR_getpriority,
    which,
    who
  );
}
#endif

#ifdef __NR_setpriority
LSW_UNUSED ssize_t setpriority(
    int which,
    int who,
    int niceval
) {
  return ::syscall::detail::syscall3(
    __NR_setpriority,
    which,
    who,
    niceval
  );
}
#endif

#ifdef __NR_sched_setparam
LSW_UNUSED ssize_t sched_setparam(
    pid_t pid,
    struct sched_param * param
) {
  return ::syscall::detail::syscall2(
    __NR_sched_setparam,
    pid,
    reinterpret_cast<ssize_t>(param)
  );
}
#endif

#ifdef __NR_sched_getparam
LSW_UNUSED ssize_t sched_getparam(
    pid_t pid,
    struct sched_param * param
) {
  return ::syscall::detail::syscall2(
    __NR_sched_getparam,
    pid,
    reinterpret_cast<ssize_t>(param)
  );
}
#endif

#ifdef __NR_sched_setscheduler
LSW_UNUSED ssize_t sched_setscheduler(
    pid_t pid,
    int policy,
    struct sched_param * param
) {
  return ::syscall::detail::syscall3(
    __NR_sched_setscheduler,
    pid,
    policy,
    reinterpret_cast<ssize_t>(param)
  );
}
#endif

#ifdef __NR_sched_getscheduler
LSW_UNUSED ssize_t sched_getscheduler(
    pid_t pid
) {
  return ::syscall::detail::syscall1(
    __NR_sched_getscheduler,
    pid
  );
}
#endif

#ifdef __NR_sched_get_priority_max
LSW_UNUSED ssize_t sched_get_priority_max(
    int policy
) {
  return ::syscall::detail::syscall1(
    __NR_sched_get_priority_max,
    policy
  );
}
#endif

#ifdef __NR_sched_get_priority_min
LSW_UNUSED ssize_t sched_get_priority_min(
    int policy
) {
  return ::syscall::detail::syscall1(
    __NR_sched_get_priority_min,
    policy
  );
}
#endif

#ifdef __NR_sched_rr_get_interval
LSW_UNUSED ssize_t sched_rr_get_interval(
    pid_t pid,
    struct __kernel_timespec * interval
) {
  return ::syscall::detail::syscall2(
    __NR_sched_rr_get_interval,
    pid,
    reinterpret_cast<ssize_t>(interval)
  );
}
#endif

#ifdef __NR_mlock
LSW_UNUSED ssize_t mlock(
    unsigned long start,
    size_t len
) {
  return ::syscall::detail::syscall2(
    __NR_mlock,
    start,
    len
  );
}
#endif

#ifdef __NR_munlock
LSW_UNUSED ssize_t munlock(
    unsigned long start,
    size_t len
) {
  return ::syscall::detail::syscall2(
    __NR_munlock,
    start,
    len
  );
}
#endif

#ifdef __NR_mlockall
LSW_UNUSED ssize_t mlockall(
    int flags
) {
  return ::syscall::detail::syscall1(
    __NR_mlockall,
    flags
  );
}
#endif

#ifdef __NR_munlockall
LSW_UNUSED ssize_t munlockall(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_munlockall,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_vhangup
LSW_UNUSED ssize_t vhangup(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_vhangup,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_modify_ldt
LSW_UNUSED ssize_t modify_ldt(
    int func,
    void * ptr,
    unsigned long bytecount
) {
  return ::syscall::detail::syscall3(
    __NR_modify_ldt,
    func,
    reinterpret_cast<ssize_t>(ptr),
    bytecount
  );
}
#endif

#ifdef __NR_pivot_root
LSW_UNUSED ssize_t pivot_root(
    const char * new_root,
    const char * put_old
) {
  return ::syscall::detail::syscall2(
    __NR_pivot_root,
    reinterpret_cast<ssize_t>(new_root),
    reinterpret_cast<ssize_t>(put_old)
  );
}
#endif

#ifdef __NR__sysctl
LSW_UNUSED ssize_t _sysctl(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR__sysctl,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_prctl
LSW_UNUSED ssize_t prctl(
    int option,
    unsigned long arg2,
    unsigned long arg3,
    unsigned long arg4,
    unsigned long arg5
) {
  return ::syscall::detail::syscall5(
    __NR_prctl,
    option,
    arg2,
    arg3,
    arg4,
    arg5
  );
}
#endif

#ifdef __NR_arch_prctl
LSW_UNUSED ssize_t arch_prctl(
    int option,
    unsigned long arg2
) {
  return ::syscall::detail::syscall2(
    __NR_arch_prctl,
    option,
    arg2
  );
}
#endif

#ifdef __NR_adjtimex
LSW_UNUSED ssize_t adjtimex(
    struct __kernel_timex * txc_p
) {
  return ::syscall::detail::syscall1(
    __NR_adjtimex,
    reinterpret_cast<ssize_t>(txc_p)
  );
}
#endif

#ifdef __NR_setrlimit
LSW_UNUSED ssize_t setrlimit(
    unsigned int resource,
    struct rlimit * rlim
) {
  return ::syscall::detail::syscall2(
    __NR_setrlimit,
    resource,
    reinterpret_cast<ssize_t>(rlim)
  );
}
#endif

#ifdef __NR_chroot
LSW_UNUSED ssize_t chroot(
    const char * filename
) {
  return ::syscall::detail::syscall1(
    __NR_chroot,
    reinterpret_cast<ssize_t>(filename)
  );
}
#endif

#ifdef __NR_sync
LSW_UNUSED ssize_t sync(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_sync,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_acct
LSW_UNUSED ssize_t acct(
    const char * name
) {
  return ::syscall::detail::syscall1(
    __NR_acct,
    reinterpret_cast<ssize_t>(name)
  );
}
#endif

#ifdef __NR_settimeofday
LSW_UNUSED ssize_t settimeofday(
    struct __kernel_old_timeval * tv,
    struct timezone * tz
) {
  return ::syscall::detail::syscall2(
    __NR_settimeofday,
    reinterpret_cast<ssize_t>(tv),
    reinterpret_cast<ssize_t>(tz)
  );
}
#endif

#ifdef __NR_mount
LSW_UNUSED ssize_t mount(
    char * dev_name,
    char * dir_name,
    char * type,
    unsigned long flags,
    void * data
) {
  return ::syscall::detail::syscall5(
    __NR_mount,
    reinterpret_cast<ssize_t>(dev_name),
    reinterpret_cast<ssize_t>(dir_name),
    reinterpret_cast<ssize_t>(type),
    flags,
    reinterpret_cast<ssize_t>(data)
  );
}
#endif

#ifdef __NR_umount2
LSW_UNUSED ssize_t umount2(
    char * name,
    int flags
) {
  return ::syscall::detail::syscall2(
    __NR_umount2,
    reinterpret_cast<ssize_t>(name),
    flags
  );
}
#endif

#ifdef __NR_swapon
LSW_UNUSED ssize_t swapon(
    const char * specialfile,
    int swap_flags
) {
  return ::syscall::detail::syscall2(
    __NR_swapon,
    reinterpret_cast<ssize_t>(specialfile),
    swap_flags
  );
}
#endif

#ifdef __NR_swapoff
LSW_UNUSED ssize_t swapoff(
    const char * specialfile
) {
  return ::syscall::detail::syscall1(
    __NR_swapoff,
    reinterpret_cast<ssize_t>(specialfile)
  );
}
#endif

#ifdef __NR_reboot
LSW_UNUSED ssize_t reboot(
    int magic1,
    int magic2,
    unsigned int cmd,
    void * arg
) {
  return ::syscall::detail::syscall4(
    __NR_reboot,
    magic1,
    magic2,
    cmd,
    reinterpret_cast<ssize_t>(arg)
  );
}
#endif

#ifdef __NR_sethostname
LSW_UNUSED ssize_t sethostname(
    char * name,
    int len
) {
  return ::syscall::detail::syscall2(
    __NR_sethostname,
    reinterpret_cast<ssize_t>(name),
    len
  );
}
#endif

#ifdef __NR_setdomainname
LSW_UNUSED ssize_t setdomainname(
    char * name,
    int len
) {
  return ::syscall::detail::syscall2(
    __NR_setdomainname,
    reinterpret_cast<ssize_t>(name),
    len
  );
}
#endif

#ifdef __NR_iopl
LSW_UNUSED ssize_t iopl(
    unsigned int level
) {
  return ::syscall::detail::syscall1(
    __NR_iopl,
    level
  );
}
#endif

#ifdef __NR_ioperm
LSW_UNUSED ssize_t ioperm(
    unsigned long from,
    unsigned long num,
    int turn_on
) {
  return ::syscall::detail::syscall3(
    __NR_ioperm,
    from,
    num,
    turn_on
  );
}
#endif

#ifdef __NR_init_module
LSW_UNUSED ssize_t init_module(
    void * umod,
    unsigned long len,
    const char * uargs
) {
  return ::syscall::detail::syscall3(
    __NR_init_module,
    reinterpret_cast<ssize_t>(umod),
    len,
    reinterpret_cast<ssize_t>(uargs)
  );
}
#endif

#ifdef __NR_delete_module
LSW_UNUSED ssize_t delete_module(
    const char * name_user,
    unsigned int flags
) {
  return ::syscall::detail::syscall2(
    __NR_delete_module,
    reinterpret_cast<ssize_t>(name_user),
    flags
  );
}
#endif

#ifdef __NR_quotactl
LSW_UNUSED ssize_t quotactl(
    unsigned int cmd,
    const char * special,
    qid_t id,
    void * addr
) {
  return ::syscall::detail::syscall4(
    __NR_quotactl,
    cmd,
    reinterpret_cast<ssize_t>(special),
    id,
    reinterpret_cast<ssize_t>(addr)
  );
}
#endif

#ifdef __NR_gettid
LSW_UNUSED ssize_t gettid(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_gettid,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_readahead
LSW_UNUSED ssize_t readahead(
    int fd,
    loff_t offset,
    size_t count
) {
  return ::syscall::detail::syscall3(
    __NR_readahead,
    fd,
    offset,
    count
  );
}
#endif

#ifdef __NR_setxattr
LSW_UNUSED ssize_t setxattr(
    const char * pathname,
    const char * name,
    const void * value,
    size_t size,
    int flags
) {
  return ::syscall::detail::syscall5(
    __NR_setxattr,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(name),
    reinterpret_cast<ssize_t>(value),
    size,
    flags
  );
}
#endif

#ifdef __NR_lsetxattr
LSW_UNUSED ssize_t lsetxattr(
    const char * pathname,
    const char * name,
    const void * value,
    size_t size,
    int flags
) {
  return ::syscall::detail::syscall5(
    __NR_lsetxattr,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(name),
    reinterpret_cast<ssize_t>(value),
    size,
    flags
  );
}
#endif

#ifdef __NR_fsetxattr
LSW_UNUSED ssize_t fsetxattr(
    int fd,
    const char * name,
    const void * value,
    size_t size,
    int flags
) {
  return ::syscall::detail::syscall5(
    __NR_fsetxattr,
    fd,
    reinterpret_cast<ssize_t>(name),
    reinterpret_cast<ssize_t>(value),
    size,
    flags
  );
}
#endif

#ifdef __NR_getxattr
LSW_UNUSED ssize_t getxattr(
    const char * pathname,
    const char * name,
    void * value,
    size_t size
) {
  return ::syscall::detail::syscall4(
    __NR_getxattr,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(name),
    reinterpret_cast<ssize_t>(value),
    size
  );
}
#endif

#ifdef __NR_lgetxattr
LSW_UNUSED ssize_t lgetxattr(
    const char * pathname,
    const char * name,
    void * value,
    size_t size
) {
  return ::syscall::detail::syscall4(
    __NR_lgetxattr,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(name),
    reinterpret_cast<ssize_t>(value),
    size
  );
}
#endif

#ifdef __NR_fgetxattr
LSW_UNUSED ssize_t fgetxattr(
    int fd,
    const char * name,
    void * value,
    size_t size
) {
  return ::syscall::detail::syscall4(
    __NR_fgetxattr,
    fd,
    reinterpret_cast<ssize_t>(name),
    reinterpret_cast<ssize_t>(value),
    size
  );
}
#endif

#ifdef __NR_listxattr
LSW_UNUSED ssize_t listxattr(
    const char * pathname,
    char * list,
    size_t size
) {
  return ::syscall::detail::syscall3(
    __NR_listxattr,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(list),
    size
  );
}
#endif

#ifdef __NR_llistxattr
LSW_UNUSED ssize_t llistxattr(
    const char * pathname,
    char * list,
    size_t size
) {
  return ::syscall::detail::syscall3(
    __NR_llistxattr,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(list),
    size
  );
}
#endif

#ifdef __NR_flistxattr
LSW_UNUSED ssize_t flistxattr(
    int fd,
    char * list,
    size_t size
) {
  return ::syscall::detail::syscall3(
    __NR_flistxattr,
    fd,
    reinterpret_cast<ssize_t>(list),
    size
  );
}
#endif

#ifdef __NR_removexattr
LSW_UNUSED ssize_t removexattr(
    const char * pathname,
    const char * name
) {
  return ::syscall::detail::syscall2(
    __NR_removexattr,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(name)
  );
}
#endif

#ifdef __NR_lremovexattr
LSW_UNUSED ssize_t lremovexattr(
    const char * pathname,
    const char * name
) {
  return ::syscall::detail::syscall2(
    __NR_lremovexattr,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(name)
  );
}
#endif

#ifdef __NR_fremovexattr
LSW_UNUSED ssize_t fremovexattr(
    int fd,
    const char * name
) {
  return ::syscall::detail::syscall2(
    __NR_fremovexattr,
    fd,
    reinterpret_cast<ssize_t>(name)
  );
}
#endif

#ifdef __NR_tkill
LSW_UNUSED ssize_t tkill(
    pid_t pid,
    int sig
) {
  return ::syscall::detail::syscall2(
    __NR_tkill,
    pid,
    sig
  );
}
#endif

#ifdef __NR_futex
LSW_UNUSED ssize_t futex(
    unsigned int uaddr,
    int op,
    unsigned int val,
    const struct __kernel_timespec * utime,
    unsigned int uaddr2,
    unsigned int val3
) {
  return ::syscall::detail::syscall6(
    __NR_futex,
    uaddr,
    op,
    val,
    reinterpret_cast<ssize_t>(utime),
    uaddr2,
    val3
  );
}
#endif

#ifdef __NR_sched_setaffinity
LSW_UNUSED ssize_t sched_setaffinity(
    pid_t pid,
    unsigned int len,
    unsigned long * user_mask_ptr
) {
  return ::syscall::detail::syscall3(
    __NR_sched_setaffinity,
    pid,
    len,
    reinterpret_cast<ssize_t>(user_mask_ptr)
  );
}
#endif

#ifdef __NR_sched_getaffinity
LSW_UNUSED ssize_t sched_getaffinity(
    pid_t pid,
    unsigned int len,
    unsigned long * user_mask_ptr
) {
  return ::syscall::detail::syscall3(
    __NR_sched_getaffinity,
    pid,
    len,
    reinterpret_cast<ssize_t>(user_mask_ptr)
  );
}
#endif

#ifdef __NR_io_setup
LSW_UNUSED ssize_t io_setup(
    unsigned int nr_events,
    aio_context_t * ctxp
) {
  return ::syscall::detail::syscall2(
    __NR_io_setup,
    nr_events,
    reinterpret_cast<ssize_t>(ctxp)
  );
}
#endif

#ifdef __NR_io_destroy
LSW_UNUSED ssize_t io_destroy(
    aio_context_t ctx
) {
  return ::syscall::detail::syscall1(
    __NR_io_destroy,
    ctx
  );
}
#endif

#ifdef __NR_io_getevents
LSW_UNUSED ssize_t io_getevents(
    aio_context_t ctx_id,
    long min_nr,
    long nr,
    struct io_event * events,
    struct __kernel_timespec * timeout
) {
  return ::syscall::detail::syscall5(
    __NR_io_getevents,
    ctx_id,
    min_nr,
    nr,
    reinterpret_cast<ssize_t>(events),
    reinterpret_cast<ssize_t>(timeout)
  );
}
#endif

#ifdef __NR_io_submit
LSW_UNUSED ssize_t io_submit(
    aio_context_t ctx_id,
    long nr,
    struct iocb ** iocbpp
) {
  return ::syscall::detail::syscall3(
    __NR_io_submit,
    ctx_id,
    nr,
    reinterpret_cast<ssize_t>(iocbpp)
  );
}
#endif

#ifdef __NR_io_cancel
LSW_UNUSED ssize_t io_cancel(
    aio_context_t ctx_id,
    struct iocb * iocb,
    struct io_event * result
) {
  return ::syscall::detail::syscall3(
    __NR_io_cancel,
    ctx_id,
    reinterpret_cast<ssize_t>(iocb),
    reinterpret_cast<ssize_t>(result)
  );
}
#endif

#ifdef __NR_epoll_create
LSW_UNUSED ssize_t epoll_create(
    int size
) {
  return ::syscall::detail::syscall1(
    __NR_epoll_create,
    size
  );
}
#endif

#ifdef __NR_remap_file_pages
LSW_UNUSED ssize_t remap_file_pages(
    unsigned long start,
    unsigned long size,
    unsigned long prot,
    unsigned long pgoff,
    unsigned long flags
) {
  return ::syscall::detail::syscall5(
    __NR_remap_file_pages,
    start,
    size,
    prot,
    pgoff,
    flags
  );
}
#endif

#ifdef __NR_getdents64
LSW_UNUSED ssize_t getdents64(
    unsigned int fd,
    struct linux_dirent64 * dirent,
    unsigned int count
) {
  return ::syscall::detail::syscall3(
    __NR_getdents64,
    fd,
    reinterpret_cast<ssize_t>(dirent),
    count
  );
}
#endif

#ifdef __NR_set_tid_address
LSW_UNUSED ssize_t set_tid_address(
    int * tidptr
) {
  return ::syscall::detail::syscall1(
    __NR_set_tid_address,
    reinterpret_cast<ssize_t>(tidptr)
  );
}
#endif

#ifdef __NR_restart_syscall
LSW_UNUSED ssize_t restart_syscall(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_restart_syscall,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_semtimedop
LSW_UNUSED ssize_t semtimedop(
    int semid,
    struct sembuf * tsops,
    unsigned int nsops,
    const struct __kernel_timespec * timeout
) {
  return ::syscall::detail::syscall4(
    __NR_semtimedop,
    semid,
    reinterpret_cast<ssize_t>(tsops),
    nsops,
    reinterpret_cast<ssize_t>(timeout)
  );
}
#endif

#ifdef __NR_fadvise64
LSW_UNUSED ssize_t fadvise64(
    int fd,
    loff_t offset,
    size_t len,
    int advice
) {
  return ::syscall::detail::syscall4(
    __NR_fadvise64,
    fd,
    offset,
    len,
    advice
  );
}
#endif

#ifdef __NR_timer_create
LSW_UNUSED ssize_t timer_create(
    const clockid_t which_clock,
    struct sigevent * timer_event_spec,
    timer_t * created_timer_id
) {
  return ::syscall::detail::syscall3(
    __NR_timer_create,
    which_clock,
    reinterpret_cast<ssize_t>(timer_event_spec),
    reinterpret_cast<ssize_t>(created_timer_id)
  );
}
#endif

#ifdef __NR_timer_settime
LSW_UNUSED ssize_t timer_settime(
    timer_t timer_id,
    int flags,
    const struct __kernel_itimerspec * new_setting,
    struct __kernel_itimerspec * old_setting
) {
  return ::syscall::detail::syscall4(
    __NR_timer_settime,
    timer_id,
    flags,
    reinterpret_cast<ssize_t>(new_setting),
    reinterpret_cast<ssize_t>(old_setting)
  );
}
#endif

#ifdef __NR_timer_gettime
LSW_UNUSED ssize_t timer_gettime(
    timer_t timer_id,
    struct __kernel_itimerspec * setting
) {
  return ::syscall::detail::syscall2(
    __NR_timer_gettime,
    timer_id,
    reinterpret_cast<ssize_t>(setting)
  );
}
#endif

#ifdef __NR_timer_getoverrun
LSW_UNUSED ssize_t timer_getoverrun(
    timer_t timer_id
) {
  return ::syscall::detail::syscall1(
    __NR_timer_getoverrun,
    timer_id
  );
}
#endif

#ifdef __NR_timer_delete
LSW_UNUSED ssize_t timer_delete(
    timer_t timer_id
) {
  return ::syscall::detail::syscall1(
    __NR_timer_delete,
    timer_id
  );
}
#endif

#ifdef __NR_clock_settime
LSW_UNUSED ssize_t clock_settime(
    const clockid_t which_clock,
    const struct __kernel_timespec * tp
) {
  return ::syscall::detail::syscall2(
    __NR_clock_settime,
    which_clock,
    reinterpret_cast<ssize_t>(tp)
  );
}
#endif

#ifdef __NR_clock_nanosleep
LSW_UNUSED ssize_t clock_nanosleep(
    const clockid_t which_clock,
    int flags,
    const struct __kernel_timespec * rqtp,
    struct __kernel_timespec * rmtp
) {
  return ::syscall::detail::syscall4(
    __NR_clock_nanosleep,
    which_clock,
    flags,
    reinterpret_cast<ssize_t>(rqtp),
    reinterpret_cast<ssize_t>(rmtp)
  );
}
#endif

#ifdef __NR_exit_group
LSW_UNUSED ssize_t exit_group(
    int error_code
) {
  return ::syscall::detail::syscall1(
    __NR_exit_group,
    error_code
  );
}
#endif

#ifdef __NR_epoll_wait
LSW_UNUSED ssize_t epoll_wait(
    int epfd,
    struct epoll_event * events,
    int maxevents,
    int timeout
) {
  return ::syscall::detail::syscall4(
    __NR_epoll_wait,
    epfd,
    reinterpret_cast<ssize_t>(events),
    maxevents,
    timeout
  );
}
#endif

#ifdef __NR_epoll_ctl
LSW_UNUSED ssize_t epoll_ctl(
    int epfd,
    int op,
    int fd,
    struct epoll_event * event
) {
  return ::syscall::detail::syscall4(
    __NR_epoll_ctl,
    epfd,
    op,
    fd,
    reinterpret_cast<ssize_t>(event)
  );
}
#endif

#ifdef __NR_tgkill
LSW_UNUSED ssize_t tgkill(
    pid_t tgid,
    pid_t pid,
    int sig
) {
  return ::syscall::detail::syscall3(
    __NR_tgkill,
    tgid,
    pid,
    sig
  );
}
#endif

#ifdef __NR_utimes
LSW_UNUSED ssize_t utimes(
    char * filename,
    struct __kernel_old_timeval * utimes
) {
  return ::syscall::detail::syscall2(
    __NR_utimes,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(utimes)
  );
}
#endif

#ifdef __NR_mbind
LSW_UNUSED ssize_t mbind(
    unsigned long start,
    unsigned long len,
    unsigned long mode,
    const unsigned long * nmask,
    unsigned long maxnode,
    unsigned int flags
) {
  return ::syscall::detail::syscall6(
    __NR_mbind,
    start,
    len,
    mode,
    reinterpret_cast<ssize_t>(nmask),
    maxnode,
    flags
  );
}
#endif

#ifdef __NR_set_mempolicy
LSW_UNUSED ssize_t set_mempolicy(
    int mode,
    const unsigned long * nmask,
    unsigned long maxnode
) {
  return ::syscall::detail::syscall3(
    __NR_set_mempolicy,
    mode,
    reinterpret_cast<ssize_t>(nmask),
    maxnode
  );
}
#endif

#ifdef __NR_get_mempolicy
LSW_UNUSED ssize_t get_mempolicy(
    int * policy,
    unsigned long * nmask,
    unsigned long maxnode,
    unsigned long addr,
    unsigned long flags
) {
  return ::syscall::detail::syscall5(
    __NR_get_mempolicy,
    reinterpret_cast<ssize_t>(policy),
    reinterpret_cast<ssize_t>(nmask),
    maxnode,
    addr,
    flags
  );
}
#endif

#ifdef __NR_mq_open
LSW_UNUSED ssize_t mq_open(
    const char * u_name,
    int oflag,
    umode_t mode,
    struct mq_attr * u_attr
) {
  return ::syscall::detail::syscall4(
    __NR_mq_open,
    reinterpret_cast<ssize_t>(u_name),
    oflag,
    mode,
    reinterpret_cast<ssize_t>(u_attr)
  );
}
#endif

#ifdef __NR_mq_unlink
LSW_UNUSED ssize_t mq_unlink(
    const char * u_name
) {
  return ::syscall::detail::syscall1(
    __NR_mq_unlink,
    reinterpret_cast<ssize_t>(u_name)
  );
}
#endif

#ifdef __NR_mq_timedsend
LSW_UNUSED ssize_t mq_timedsend(
    mqd_t mqdes,
    const char * u_msg_ptr,
    size_t msg_len,
    unsigned int msg_prio,
    const struct __kernel_timespec * u_abs_timeout
) {
  return ::syscall::detail::syscall5(
    __NR_mq_timedsend,
    mqdes,
    reinterpret_cast<ssize_t>(u_msg_ptr),
    msg_len,
    msg_prio,
    reinterpret_cast<ssize_t>(u_abs_timeout)
  );
}
#endif

#ifdef __NR_mq_timedreceive
LSW_UNUSED ssize_t mq_timedreceive(
    mqd_t mqdes,
    char * u_msg_ptr,
    size_t msg_len,
    unsigned int * u_msg_prio,
    const struct __kernel_timespec * u_abs_timeout
) {
  return ::syscall::detail::syscall5(
    __NR_mq_timedreceive,
    mqdes,
    reinterpret_cast<ssize_t>(u_msg_ptr),
    msg_len,
    reinterpret_cast<ssize_t>(u_msg_prio),
    reinterpret_cast<ssize_t>(u_abs_timeout)
  );
}
#endif

#ifdef __NR_mq_notify
LSW_UNUSED ssize_t mq_notify(
    mqd_t mqdes,
    const struct sigevent * u_notification
) {
  return ::syscall::detail::syscall2(
    __NR_mq_notify,
    mqdes,
    reinterpret_cast<ssize_t>(u_notification)
  );
}
#endif

#ifdef __NR_mq_getsetattr
LSW_UNUSED ssize_t mq_getsetattr(
    mqd_t mqdes,
    const struct mq_attr * u_mqstat,
    struct mq_attr * u_omqstat
) {
  return ::syscall::detail::syscall3(
    __NR_mq_getsetattr,
    mqdes,
    reinterpret_cast<ssize_t>(u_mqstat),
    reinterpret_cast<ssize_t>(u_omqstat)
  );
}
#endif

#ifdef __NR_kexec_load
LSW_UNUSED ssize_t kexec_load(
    unsigned long entry,
    unsigned long nr_segments,
    struct kexec_segment * segments,
    unsigned long flags
) {
  return ::syscall::detail::syscall4(
    __NR_kexec_load,
    entry,
    nr_segments,
    reinterpret_cast<ssize_t>(segments),
    flags
  );
}
#endif

#ifdef __NR_waitid
LSW_UNUSED ssize_t waitid(
    int which,
    pid_t upid,
    struct siginfo * infop,
    int options,
    struct rusage * ru
) {
  return ::syscall::detail::syscall5(
    __NR_waitid,
    which,
    upid,
    reinterpret_cast<ssize_t>(infop),
    options,
    reinterpret_cast<ssize_t>(ru)
  );
}
#endif

#ifdef __NR_add_key
LSW_UNUSED ssize_t add_key(
    const char * _type,
    const char * _description,
    const void * _payload,
    size_t plen,
    key_serial_t ringid
) {
  return ::syscall::detail::syscall5(
    __NR_add_key,
    reinterpret_cast<ssize_t>(_type),
    reinterpret_cast<ssize_t>(_description),
    reinterpret_cast<ssize_t>(_payload),
    plen,
    ringid
  );
}
#endif

#ifdef __NR_request_key
LSW_UNUSED ssize_t request_key(
    const char * _type,
    const char * _description,
    const char * _callout_info,
    key_serial_t destringid
) {
  return ::syscall::detail::syscall4(
    __NR_request_key,
    reinterpret_cast<ssize_t>(_type),
    reinterpret_cast<ssize_t>(_description),
    reinterpret_cast<ssize_t>(_callout_info),
    destringid
  );
}
#endif

#ifdef __NR_keyctl
LSW_UNUSED ssize_t keyctl(
    int option,
    unsigned long arg2,
    unsigned long arg3,
    unsigned long arg4,
    unsigned long arg5
) {
  return ::syscall::detail::syscall5(
    __NR_keyctl,
    option,
    arg2,
    arg3,
    arg4,
    arg5
  );
}
#endif

#ifdef __NR_ioprio_set
LSW_UNUSED ssize_t ioprio_set(
    int which,
    int who,
    int ioprio
) {
  return ::syscall::detail::syscall3(
    __NR_ioprio_set,
    which,
    who,
    ioprio
  );
}
#endif

#ifdef __NR_ioprio_get
LSW_UNUSED ssize_t ioprio_get(
    int which,
    int who
) {
  return ::syscall::detail::syscall2(
    __NR_ioprio_get,
    which,
    who
  );
}
#endif

#ifdef __NR_inotify_init
LSW_UNUSED ssize_t inotify_init(
    const struct pt_regs * __unused
) {
  return ::syscall::detail::syscall1(
    __NR_inotify_init,
    reinterpret_cast<ssize_t>(__unused)
  );
}
#endif

#ifdef __NR_inotify_add_watch
LSW_UNUSED ssize_t inotify_add_watch(
    int fd,
    const char * pathname,
    unsigned int mask
) {
  return ::syscall::detail::syscall3(
    __NR_inotify_add_watch,
    fd,
    reinterpret_cast<ssize_t>(pathname),
    mask
  );
}
#endif

#ifdef __NR_inotify_rm_watch
LSW_UNUSED ssize_t inotify_rm_watch(
    int fd,
    int wd
) {
  return ::syscall::detail::syscall2(
    __NR_inotify_rm_watch,
    fd,
    wd
  );
}
#endif

#ifdef __NR_migrate_pages
LSW_UNUSED ssize_t migrate_pages(
    pid_t pid,
    unsigned long maxnode,
    const unsigned long * old_nodes,
    const unsigned long * new_nodes
) {
  return ::syscall::detail::syscall4(
    __NR_migrate_pages,
    pid,
    maxnode,
    reinterpret_cast<ssize_t>(old_nodes),
    reinterpret_cast<ssize_t>(new_nodes)
  );
}
#endif

#ifdef __NR_openat
LSW_UNUSED ssize_t openat(
    int dfd,
    const char * filename,
    int flags,
    umode_t mode
) {
  return ::syscall::detail::syscall4(
    __NR_openat,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    flags,
    mode
  );
}
#endif

#ifdef __NR_mkdirat
LSW_UNUSED ssize_t mkdirat(
    int dfd,
    const char * pathname,
    umode_t mode
) {
  return ::syscall::detail::syscall3(
    __NR_mkdirat,
    dfd,
    reinterpret_cast<ssize_t>(pathname),
    mode
  );
}
#endif

#ifdef __NR_mknodat
LSW_UNUSED ssize_t mknodat(
    int dfd,
    const char * filename,
    umode_t mode,
    unsigned int dev
) {
  return ::syscall::detail::syscall4(
    __NR_mknodat,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    mode,
    dev
  );
}
#endif

#ifdef __NR_fchownat
LSW_UNUSED ssize_t fchownat(
    int dfd,
    const char * filename,
    uid_t user,
    gid_t group,
    int flag
) {
  return ::syscall::detail::syscall5(
    __NR_fchownat,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    user,
    group,
    flag
  );
}
#endif

#ifdef __NR_futimesat
LSW_UNUSED ssize_t futimesat(
    int dfd,
    const char * filename,
    struct __kernel_old_timeval * utimes
) {
  return ::syscall::detail::syscall3(
    __NR_futimesat,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(utimes)
  );
}
#endif

#ifdef __NR_newfstatat
LSW_UNUSED ssize_t newfstatat(
    int dfd,
    const char * filename,
    struct stat * statbuf,
    int flag
) {
  return ::syscall::detail::syscall4(
    __NR_newfstatat,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(statbuf),
    flag
  );
}
#endif

#ifdef __NR_unlinkat
LSW_UNUSED ssize_t unlinkat(
    int dfd,
    const char * pathname,
    int flag
) {
  return ::syscall::detail::syscall3(
    __NR_unlinkat,
    dfd,
    reinterpret_cast<ssize_t>(pathname),
    flag
  );
}
#endif

#ifdef __NR_renameat
LSW_UNUSED ssize_t renameat(
    int olddfd,
    const char * oldname,
    int newdfd,
    const char * newname
) {
  return ::syscall::detail::syscall4(
    __NR_renameat,
    olddfd,
    reinterpret_cast<ssize_t>(oldname),
    newdfd,
    reinterpret_cast<ssize_t>(newname)
  );
}
#endif

#ifdef __NR_linkat
LSW_UNUSED ssize_t linkat(
    int olddfd,
    const char * oldname,
    int newdfd,
    const char * newname,
    int flags
) {
  return ::syscall::detail::syscall5(
    __NR_linkat,
    olddfd,
    reinterpret_cast<ssize_t>(oldname),
    newdfd,
    reinterpret_cast<ssize_t>(newname),
    flags
  );
}
#endif

#ifdef __NR_symlinkat
LSW_UNUSED ssize_t symlinkat(
    const char * oldname,
    int newdfd,
    const char * newname
) {
  return ::syscall::detail::syscall3(
    __NR_symlinkat,
    reinterpret_cast<ssize_t>(oldname),
    newdfd,
    reinterpret_cast<ssize_t>(newname)
  );
}
#endif

#ifdef __NR_readlinkat
LSW_UNUSED ssize_t readlinkat(
    int dfd,
    const char * pathname,
    char * buf,
    int bufsiz
) {
  return ::syscall::detail::syscall4(
    __NR_readlinkat,
    dfd,
    reinterpret_cast<ssize_t>(pathname),
    reinterpret_cast<ssize_t>(buf),
    bufsiz
  );
}
#endif

#ifdef __NR_fchmodat
LSW_UNUSED ssize_t fchmodat(
    int dfd,
    const char * filename,
    umode_t mode
) {
  return ::syscall::detail::syscall3(
    __NR_fchmodat,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    mode
  );
}
#endif

#ifdef __NR_faccessat
LSW_UNUSED ssize_t faccessat(
    int dfd,
    const char * filename,
    int mode
) {
  return ::syscall::detail::syscall3(
    __NR_faccessat,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    mode
  );
}
#endif

#ifdef __NR_pselect6
LSW_UNUSED ssize_t pselect6(
    int n,
    fd_set * inp,
    fd_set * outp,
    fd_set * exp,
    struct __kernel_timespec * tsp,
    void * sig
) {
  return ::syscall::detail::syscall6(
    __NR_pselect6,
    n,
    reinterpret_cast<ssize_t>(inp),
    reinterpret_cast<ssize_t>(outp),
    reinterpret_cast<ssize_t>(exp),
    reinterpret_cast<ssize_t>(tsp),
    reinterpret_cast<ssize_t>(sig)
  );
}
#endif

#ifdef __NR_ppoll
LSW_UNUSED ssize_t ppoll(
    struct pollfd * ufds,
    unsigned int nfds,
    struct __kernel_timespec * tsp,
    const sigset_t * sigmask,
    size_t sigsetsize
) {
  return ::syscall::detail::syscall5(
    __NR_ppoll,
    reinterpret_cast<ssize_t>(ufds),
    nfds,
    reinterpret_cast<ssize_t>(tsp),
    reinterpret_cast<ssize_t>(sigmask),
    sigsetsize
  );
}
#endif

#ifdef __NR_unshare
LSW_UNUSED ssize_t unshare(
    unsigned long unshare_flags
) {
  return ::syscall::detail::syscall1(
    __NR_unshare,
    unshare_flags
  );
}
#endif

#ifdef __NR_set_robust_list
LSW_UNUSED ssize_t set_robust_list(
    struct robust_list_head * head,
    size_t len
) {
  return ::syscall::detail::syscall2(
    __NR_set_robust_list,
    reinterpret_cast<ssize_t>(head),
    len
  );
}
#endif

#ifdef __NR_get_robust_list
LSW_UNUSED ssize_t get_robust_list(
    int pid,
    struct robust_list_head ** head_ptr,
    size_t * len_ptr
) {
  return ::syscall::detail::syscall3(
    __NR_get_robust_list,
    pid,
    reinterpret_cast<ssize_t>(head_ptr),
    reinterpret_cast<ssize_t>(len_ptr)
  );
}
#endif

#ifdef __NR_splice
LSW_UNUSED ssize_t splice(
    int fd_in,
    loff_t * off_in,
    int fd_out,
    loff_t * off_out,
    size_t len,
    unsigned int flags
) {
  return ::syscall::detail::syscall6(
    __NR_splice,
    fd_in,
    reinterpret_cast<ssize_t>(off_in),
    fd_out,
    reinterpret_cast<ssize_t>(off_out),
    len,
    flags
  );
}
#endif

#ifdef __NR_tee
LSW_UNUSED ssize_t tee(
    int fdin,
    int fdout,
    size_t len,
    unsigned int flags
) {
  return ::syscall::detail::syscall4(
    __NR_tee,
    fdin,
    fdout,
    len,
    flags
  );
}
#endif

#ifdef __NR_sync_file_range
LSW_UNUSED ssize_t sync_file_range(
    int fd,
    loff_t offset,
    loff_t nbytes,
    unsigned int flags
) {
  return ::syscall::detail::syscall4(
    __NR_sync_file_range,
    fd,
    offset,
    nbytes,
    flags
  );
}
#endif

#ifdef __NR_vmsplice
LSW_UNUSED ssize_t vmsplice(
    int fd,
    const struct iovec * uiov,
    unsigned long nr_segs,
    unsigned int flags
) {
  return ::syscall::detail::syscall4(
    __NR_vmsplice,
    fd,
    reinterpret_cast<ssize_t>(uiov),
    nr_segs,
    flags
  );
}
#endif

#ifdef __NR_move_pages
LSW_UNUSED ssize_t move_pages(
    pid_t pid,
    unsigned long nr_pages,
    const void ** pages,
    const int * nodes,
    int * status,
    int flags
) {
  return ::syscall::detail::syscall6(
    __NR_move_pages,
    pid,
    nr_pages,
    reinterpret_cast<ssize_t>(pages),
    reinterpret_cast<ssize_t>(nodes),
    reinterpret_cast<ssize_t>(status),
    flags
  );
}
#endif

#ifdef __NR_utimensat
LSW_UNUSED ssize_t utimensat(
    int dfd,
    const char * filename,
    struct __kernel_timespec * utimes,
    int flags
) {
  return ::syscall::detail::syscall4(
    __NR_utimensat,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(utimes),
    flags
  );
}
#endif

#ifdef __NR_epoll_pwait
LSW_UNUSED ssize_t epoll_pwait(
    int epfd,
    struct epoll_event * events,
    int maxevents,
    int timeout,
    const sigset_t * sigmask,
    size_t sigsetsize
) {
  return ::syscall::detail::syscall6(
    __NR_epoll_pwait,
    epfd,
    reinterpret_cast<ssize_t>(events),
    maxevents,
    timeout,
    reinterpret_cast<ssize_t>(sigmask),
    sigsetsize
  );
}
#endif

#ifdef __NR_signalfd
LSW_UNUSED ssize_t signalfd(
    int ufd,
    sigset_t * user_mask,
    size_t sizemask
) {
  return ::syscall::detail::syscall3(
    __NR_signalfd,
    ufd,
    reinterpret_cast<ssize_t>(user_mask),
    sizemask
  );
}
#endif

#ifdef __NR_timerfd_create
LSW_UNUSED ssize_t timerfd_create(
    int clockid,
    int flags
) {
  return ::syscall::detail::syscall2(
    __NR_timerfd_create,
    clockid,
    flags
  );
}
#endif

#ifdef __NR_eventfd
LSW_UNUSED ssize_t eventfd(
    unsigned int count
) {
  return ::syscall::detail::syscall1(
    __NR_eventfd,
    count
  );
}
#endif

#ifdef __NR_fallocate
LSW_UNUSED ssize_t fallocate(
    int fd,
    int mode,
    loff_t offset,
    loff_t len
) {
  return ::syscall::detail::syscall4(
    __NR_fallocate,
    fd,
    mode,
    offset,
    len
  );
}
#endif

#ifdef __NR_timerfd_settime
LSW_UNUSED ssize_t timerfd_settime(
    int ufd,
    int flags,
    const struct __kernel_itimerspec * utmr,
    struct __kernel_itimerspec * otmr
) {
  return ::syscall::detail::syscall4(
    __NR_timerfd_settime,
    ufd,
    flags,
    reinterpret_cast<ssize_t>(utmr),
    reinterpret_cast<ssize_t>(otmr)
  );
}
#endif

#ifdef __NR_timerfd_gettime
LSW_UNUSED ssize_t timerfd_gettime(
    int ufd,
    struct __kernel_itimerspec * otmr
) {
  return ::syscall::detail::syscall2(
    __NR_timerfd_gettime,
    ufd,
    reinterpret_cast<ssize_t>(otmr)
  );
}
#endif

#ifdef __NR_accept4
LSW_UNUSED ssize_t accept4(
    int fd,
    struct sockaddr * upeer_sockaddr,
    int * upeer_addrlen,
    int flags
) {
  return ::syscall::detail::syscall4(
    __NR_accept4,
    fd,
    reinterpret_cast<ssize_t>(upeer_sockaddr),
    reinterpret_cast<ssize_t>(upeer_addrlen),
    flags
  );
}
#endif

#ifdef __NR_signalfd4
LSW_UNUSED ssize_t signalfd4(
    int ufd,
    sigset_t * user_mask,
    size_t sizemask,
    int flags
) {
  return ::syscall::detail::syscall4(
    __NR_signalfd4,
    ufd,
    reinterpret_cast<ssize_t>(user_mask),
    sizemask,
    flags
  );
}
#endif

#ifdef __NR_eventfd2
LSW_UNUSED ssize_t eventfd2(
    unsigned int count,
    int flags
) {
  return ::syscall::detail::syscall2(
    __NR_eventfd2,
    count,
    flags
  );
}
#endif

#ifdef __NR_epoll_create1
LSW_UNUSED ssize_t epoll_create1(
    int flags
) {
  return ::syscall::detail::syscall1(
    __NR_epoll_create1,
    flags
  );
}
#endif

#ifdef __NR_dup3
LSW_UNUSED ssize_t dup3(
    unsigned int oldfd,
    unsigned int newfd,
    int flags
) {
  return ::syscall::detail::syscall3(
    __NR_dup3,
    oldfd,
    newfd,
    flags
  );
}
#endif

#ifdef __NR_pipe2
LSW_UNUSED ssize_t pipe2(
    int * fildes,
    int flags
) {
  return ::syscall::detail::syscall2(
    __NR_pipe2,
    reinterpret_cast<ssize_t>(fildes),
    flags
  );
}
#endif

#ifdef __NR_inotify_init1
LSW_UNUSED ssize_t inotify_init1(
    int flags
) {
  return ::syscall::detail::syscall1(
    __NR_inotify_init1,
    flags
  );
}
#endif

#ifdef __NR_preadv
LSW_UNUSED ssize_t preadv(
    unsigned long fd,
    const struct iovec * vec,
    unsigned long vlen,
    unsigned long pos_l,
    unsigned long pos_h
) {
  return ::syscall::detail::syscall5(
    __NR_preadv,
    fd,
    reinterpret_cast<ssize_t>(vec),
    vlen,
    pos_l,
    pos_h
  );
}
#endif

#ifdef __NR_pwritev
LSW_UNUSED ssize_t pwritev(
    unsigned long fd,
    const struct iovec * vec,
    unsigned long vlen,
    unsigned long pos_l,
    unsigned long pos_h
) {
  return ::syscall::detail::syscall5(
    __NR_pwritev,
    fd,
    reinterpret_cast<ssize_t>(vec),
    vlen,
    pos_l,
    pos_h
  );
}
#endif

#ifdef __NR_rt_tgsigqueueinfo
LSW_UNUSED ssize_t rt_tgsigqueueinfo(
    pid_t tgid,
    pid_t pid,
    int sig,
    siginfo_t * uinfo
) {
  return ::syscall::detail::syscall4(
    __NR_rt_tgsigqueueinfo,
    tgid,
    pid,
    sig,
    reinterpret_cast<ssize_t>(uinfo)
  );
}
#endif

#ifdef __NR_perf_event_open
LSW_UNUSED ssize_t perf_event_open(
    struct perf_event_attr * attr_uptr,
    pid_t pid,
    int cpu,
    int group_fd,
    unsigned long flags
) {
  return ::syscall::detail::syscall5(
    __NR_perf_event_open,
    reinterpret_cast<ssize_t>(attr_uptr),
    pid,
    cpu,
    group_fd,
    flags
  );
}
#endif

#ifdef __NR_recvmmsg
LSW_UNUSED ssize_t recvmmsg(
    int fd,
    struct mmsghdr * mmsg,
    unsigned int vlen,
    unsigned int flags,
    struct __kernel_timespec * timeout
) {
  return ::syscall::detail::syscall5(
    __NR_recvmmsg,
    fd,
    reinterpret_cast<ssize_t>(mmsg),
    vlen,
    flags,
    reinterpret_cast<ssize_t>(timeout)
  );
}
#endif

#ifdef __NR_fanotify_init
LSW_UNUSED ssize_t fanotify_init(
    unsigned int flags,
    unsigned int event_f_flags
) {
  return ::syscall::detail::syscall2(
    __NR_fanotify_init,
    flags,
    event_f_flags
  );
}
#endif

#ifdef __NR_fanotify_mark
LSW_UNUSED ssize_t fanotify_mark(
    int fanotify_fd,
    unsigned int flags,
    __u64 mask,
    int dfd,
    const char * pathname
) {
  return ::syscall::detail::syscall5(
    __NR_fanotify_mark,
    fanotify_fd,
    flags,
    mask,
    dfd,
    reinterpret_cast<ssize_t>(pathname)
  );
}
#endif

#ifdef __NR_prlimit64
LSW_UNUSED ssize_t prlimit64(
    pid_t pid,
    unsigned int resource,
    const struct rlimit64 * new_rlim,
    struct rlimit64 * old_rlim
) {
  return ::syscall::detail::syscall4(
    __NR_prlimit64,
    pid,
    resource,
    reinterpret_cast<ssize_t>(new_rlim),
    reinterpret_cast<ssize_t>(old_rlim)
  );
}
#endif

#ifdef __NR_name_to_handle_at
LSW_UNUSED ssize_t name_to_handle_at(
    int dfd,
    const char * name,
    struct file_handle * handle,
    int * mnt_id,
    int flag
) {
  return ::syscall::detail::syscall5(
    __NR_name_to_handle_at,
    dfd,
    reinterpret_cast<ssize_t>(name),
    reinterpret_cast<ssize_t>(handle),
    reinterpret_cast<ssize_t>(mnt_id),
    flag
  );
}
#endif

#ifdef __NR_open_by_handle_at
LSW_UNUSED ssize_t open_by_handle_at(
    int mountdirfd,
    struct file_handle * handle,
    int flags
) {
  return ::syscall::detail::syscall3(
    __NR_open_by_handle_at,
    mountdirfd,
    reinterpret_cast<ssize_t>(handle),
    flags
  );
}
#endif

#ifdef __NR_clock_adjtime
LSW_UNUSED ssize_t clock_adjtime(
    const clockid_t which_clock,
    struct __kernel_timex * utx
) {
  return ::syscall::detail::syscall2(
    __NR_clock_adjtime,
    which_clock,
    reinterpret_cast<ssize_t>(utx)
  );
}
#endif

#ifdef __NR_syncfs
LSW_UNUSED ssize_t syncfs(
    int fd
) {
  return ::syscall::detail::syscall1(
    __NR_syncfs,
    fd
  );
}
#endif

#ifdef __NR_sendmmsg
LSW_UNUSED ssize_t sendmmsg(
    int fd,
    struct mmsghdr * mmsg,
    unsigned int vlen,
    unsigned int flags
) {
  return ::syscall::detail::syscall4(
    __NR_sendmmsg,
    fd,
    reinterpret_cast<ssize_t>(mmsg),
    vlen,
    flags
  );
}
#endif

#ifdef __NR_setns
LSW_UNUSED ssize_t setns(
    int fd,
    int flags
) {
  return ::syscall::detail::syscall2(
    __NR_setns,
    fd,
    flags
  );
}
#endif

#ifdef __NR_process_vm_readv
LSW_UNUSED ssize_t process_vm_readv(
    pid_t pid,
    const struct iovec * lvec,
    unsigned long liovcnt,
    const struct iovec * rvec,
    unsigned long riovcnt,
    unsigned long flags
) {
  return ::syscall::detail::syscall6(
    __NR_process_vm_readv,
    pid,
    reinterpret_cast<ssize_t>(lvec),
    liovcnt,
    reinterpret_cast<ssize_t>(rvec),
    riovcnt,
    flags
  );
}
#endif

#ifdef __NR_process_vm_writev
LSW_UNUSED ssize_t process_vm_writev(
    pid_t pid,
    const struct iovec * lvec,
    unsigned long liovcnt,
    const struct iovec * rvec,
    unsigned long riovcnt,
    unsigned long flags
) {
  return ::syscall::detail::syscall6(
    __NR_process_vm_writev,
    pid,
    reinterpret_cast<ssize_t>(lvec),
    liovcnt,
    reinterpret_cast<ssize_t>(rvec),
    riovcnt,
    flags
  );
}
#endif

#ifdef __NR_kcmp
LSW_UNUSED ssize_t kcmp(
    pid_t pid1,
    pid_t pid2,
    int type,
    unsigned long idx1,
    unsigned long idx2
) {
  return ::syscall::detail::syscall5(
    __NR_kcmp,
    pid1,
    pid2,
    type,
    idx1,
    idx2
  );
}
#endif

#ifdef __NR_finit_module
LSW_UNUSED ssize_t finit_module(
    int fd,
    const char * uargs,
    int flags
) {
  return ::syscall::detail::syscall3(
    __NR_finit_module,
    fd,
    reinterpret_cast<ssize_t>(uargs),
    flags
  );
}
#endif

#ifdef __NR_sched_setattr
LSW_UNUSED ssize_t sched_setattr(
    pid_t pid,
    struct sched_attr * uattr,
    unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_sched_setattr,
    pid,
    reinterpret_cast<ssize_t>(uattr),
    flags
  );
}
#endif

#ifdef __NR_sched_getattr
LSW_UNUSED ssize_t sched_getattr(
    pid_t pid,
    struct sched_attr * uattr,
    unsigned int usize,
    unsigned int flags
) {
  return ::syscall::detail::syscall4(
    __NR_sched_getattr,
    pid,
    reinterpret_cast<ssize_t>(uattr),
    usize,
    flags
  );
}
#endif

#ifdef __NR_renameat2
LSW_UNUSED ssize_t renameat2(
    int olddfd,
    const char * oldname,
    int newdfd,
    const char * newname,
    unsigned int flags
) {
  return ::syscall::detail::syscall5(
    __NR_renameat2,
    olddfd,
    reinterpret_cast<ssize_t>(oldname),
    newdfd,
    reinterpret_cast<ssize_t>(newname),
    flags
  );
}
#endif

#ifdef __NR_seccomp
LSW_UNUSED ssize_t seccomp(
    unsigned int op,
    unsigned int flags,
    void * uargs
) {
  return ::syscall::detail::syscall3(
    __NR_seccomp,
    op,
    flags,
    reinterpret_cast<ssize_t>(uargs)
  );
}
#endif

#ifdef __NR_getrandom
LSW_UNUSED ssize_t getrandom(
    char * ubuf,
    size_t len,
    unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_getrandom,
    reinterpret_cast<ssize_t>(ubuf),
    len,
    flags
  );
}
#endif

#ifdef __NR_memfd_create
LSW_UNUSED ssize_t memfd_create(
    const char * uname,
    unsigned int flags
) {
  return ::syscall::detail::syscall2(
    __NR_memfd_create,
    reinterpret_cast<ssize_t>(uname),
    flags
  );
}
#endif

#ifdef __NR_kexec_file_load
LSW_UNUSED ssize_t kexec_file_load(
    int kernel_fd,
    int initrd_fd,
    unsigned long cmdline_len,
    const char * cmdline_ptr,
    unsigned long flags
) {
  return ::syscall::detail::syscall5(
    __NR_kexec_file_load,
    kernel_fd,
    initrd_fd,
    cmdline_len,
    reinterpret_cast<ssize_t>(cmdline_ptr),
    flags
  );
}
#endif

#ifdef __NR_bpf
LSW_UNUSED ssize_t bpf(
    int cmd,
    union bpf_attr * uattr,
    unsigned int size
) {
  return ::syscall::detail::syscall3(
    __NR_bpf,
    cmd,
    reinterpret_cast<ssize_t>(uattr),
    size
  );
}
#endif

#ifdef __NR_execveat
LSW_UNUSED ssize_t execveat(
    int fd,
    const char * filename,
    const char *const * argv,
    const char *const * envp,
    int flags
) {
  return ::syscall::detail::syscall5(
    __NR_execveat,
    fd,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(argv),
    reinterpret_cast<ssize_t>(envp),
    flags
  );
}
#endif

#ifdef __NR_userfaultfd
LSW_UNUSED ssize_t userfaultfd(
    int flags
) {
  return ::syscall::detail::syscall1(
    __NR_userfaultfd,
    flags
  );
}
#endif

#ifdef __NR_membarrier
LSW_UNUSED ssize_t membarrier(
    int cmd,
    unsigned int flags,
    int cpu_id
) {
  return ::syscall::detail::syscall3(
    __NR_membarrier,
    cmd,
    flags,
    cpu_id
  );
}
#endif

#ifdef __NR_mlock2
LSW_UNUSED ssize_t mlock2(
    unsigned long start,
    size_t len,
    int flags
) {
  return ::syscall::detail::syscall3(
    __NR_mlock2,
    start,
    len,
    flags
  );
}
#endif

#ifdef __NR_copy_file_range
LSW_UNUSED ssize_t copy_file_range(
    int fd_in,
    loff_t * off_in,
    int fd_out,
    loff_t * off_out,
    size_t len,
    unsigned int flags
) {
  return ::syscall::detail::syscall6(
    __NR_copy_file_range,
    fd_in,
    reinterpret_cast<ssize_t>(off_in),
    fd_out,
    reinterpret_cast<ssize_t>(off_out),
    len,
    flags
  );
}
#endif

#ifdef __NR_preadv2
LSW_UNUSED ssize_t preadv2(
    unsigned long fd,
    const struct iovec * vec,
    unsigned long vlen,
    unsigned long pos_l,
    unsigned long pos_h,
    rwf_t flags
) {
  return ::syscall::detail::syscall6(
    __NR_preadv2,
    fd,
    reinterpret_cast<ssize_t>(vec),
    vlen,
    pos_l,
    pos_h,
    flags
  );
}
#endif

#ifdef __NR_pwritev2
LSW_UNUSED ssize_t pwritev2(
    unsigned long fd,
    const struct iovec * vec,
    unsigned long vlen,
    unsigned long pos_l,
    unsigned long pos_h,
    rwf_t flags
) {
  return ::syscall::detail::syscall6(
    __NR_pwritev2,
    fd,
    reinterpret_cast<ssize_t>(vec),
    vlen,
    pos_l,
    pos_h,
    flags
  );
}
#endif

#ifdef __NR_pkey_mprotect
LSW_UNUSED ssize_t pkey_mprotect(
    unsigned long start,
    size_t len,
    unsigned long prot,
    int pkey
) {
  return ::syscall::detail::syscall4(
    __NR_pkey_mprotect,
    start,
    len,
    prot,
    pkey
  );
}
#endif

#ifdef __NR_pkey_alloc
LSW_UNUSED ssize_t pkey_alloc(
    unsigned long flags,
    unsigned long init_val
) {
  return ::syscall::detail::syscall2(
    __NR_pkey_alloc,
    flags,
    init_val
  );
}
#endif

#ifdef __NR_pkey_free
LSW_UNUSED ssize_t pkey_free(
    int pkey
) {
  return ::syscall::detail::syscall1(
    __NR_pkey_free,
    pkey
  );
}
#endif

#ifdef __NR_statx
LSW_UNUSED ssize_t statx(
    int dfd,
    const char * filename,
    unsigned int flags,
    unsigned int mask,
    struct statx * buffer
) {
  return ::syscall::detail::syscall5(
    __NR_statx,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    flags,
    mask,
    reinterpret_cast<ssize_t>(buffer)
  );
}
#endif

#ifdef __NR_io_pgetevents
LSW_UNUSED ssize_t io_pgetevents(
    aio_context_t ctx_id,
    long min_nr,
    long nr,
    struct io_event * events,
    struct __kernel_timespec * timeout,
    const struct __aio_sigset * usig
) {
  return ::syscall::detail::syscall6(
    __NR_io_pgetevents,
    ctx_id,
    min_nr,
    nr,
    reinterpret_cast<ssize_t>(events),
    reinterpret_cast<ssize_t>(timeout),
    reinterpret_cast<ssize_t>(usig)
  );
}
#endif

#ifdef __NR_rseq
LSW_UNUSED ssize_t rseq(
    struct rseq * rseq,
    unsigned int rseq_len,
    int flags,
    unsigned int sig
) {
  return ::syscall::detail::syscall4(
    __NR_rseq,
    reinterpret_cast<ssize_t>(rseq),
    rseq_len,
    flags,
    sig
  );
}
#endif

#ifdef __NR_pidfd_send_signal
LSW_UNUSED ssize_t pidfd_send_signal(
    int pidfd,
    int sig,
    siginfo_t * info,
    unsigned int flags
) {
  return ::syscall::detail::syscall4(
    __NR_pidfd_send_signal,
    pidfd,
    sig,
    reinterpret_cast<ssize_t>(info),
    flags
  );
}
#endif

#ifdef __NR_io_uring_setup
LSW_UNUSED ssize_t io_uring_setup(
    unsigned int entries,
    struct io_uring_params * params
) {
  return ::syscall::detail::syscall2(
    __NR_io_uring_setup,
    entries,
    reinterpret_cast<ssize_t>(params)
  );
}
#endif

#ifdef __NR_io_uring_enter
LSW_UNUSED ssize_t io_uring_enter(
    unsigned int fd,
    unsigned int to_submit,
    unsigned int min_complete,
    unsigned int flags,
    const void * argp,
    size_t argsz
) {
  return ::syscall::detail::syscall6(
    __NR_io_uring_enter,
    fd,
    to_submit,
    min_complete,
    flags,
    reinterpret_cast<ssize_t>(argp),
    argsz
  );
}
#endif

#ifdef __NR_io_uring_register
LSW_UNUSED ssize_t io_uring_register(
    unsigned int fd,
    unsigned int opcode,
    void * arg,
    unsigned int nr_args
) {
  return ::syscall::detail::syscall4(
    __NR_io_uring_register,
    fd,
    opcode,
    reinterpret_cast<ssize_t>(arg),
    nr_args
  );
}
#endif

#ifdef __NR_open_tree
LSW_UNUSED ssize_t open_tree(
    int dfd,
    const char * filename,
    unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_open_tree,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    flags
  );
}
#endif

#ifdef __NR_move_mount
LSW_UNUSED ssize_t move_mount(
    int from_dfd,
    const char * from_pathname,
    int to_dfd,
    const char * to_pathname,
    unsigned int flags
) {
  return ::syscall::detail::syscall5(
    __NR_move_mount,
    from_dfd,
    reinterpret_cast<ssize_t>(from_pathname),
    to_dfd,
    reinterpret_cast<ssize_t>(to_pathname),
    flags
  );
}
#endif

#ifdef __NR_fsopen
LSW_UNUSED ssize_t fsopen(
    const char * _fs_name,
    unsigned int flags
) {
  return ::syscall::detail::syscall2(
    __NR_fsopen,
    reinterpret_cast<ssize_t>(_fs_name),
    flags
  );
}
#endif

#ifdef __NR_fsconfig
LSW_UNUSED ssize_t fsconfig(
    int fd,
    unsigned int cmd,
    const char * _key,
    const void * _value,
    int aux
) {
  return ::syscall::detail::syscall5(
    __NR_fsconfig,
    fd,
    cmd,
    reinterpret_cast<ssize_t>(_key),
    reinterpret_cast<ssize_t>(_value),
    aux
  );
}
#endif

#ifdef __NR_fsmount
LSW_UNUSED ssize_t fsmount(
    int fs_fd,
    unsigned int flags,
    unsigned int attr_flags
) {
  return ::syscall::detail::syscall3(
    __NR_fsmount,
    fs_fd,
    flags,
    attr_flags
  );
}
#endif

#ifdef __NR_fspick
LSW_UNUSED ssize_t fspick(
    int dfd,
    const char * path,
    unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_fspick,
    dfd,
    reinterpret_cast<ssize_t>(path),
    flags
  );
}
#endif

#ifdef __NR_pidfd_open
LSW_UNUSED ssize_t pidfd_open(
    pid_t pid,
    unsigned int flags
) {
  return ::syscall::detail::syscall2(
    __NR_pidfd_open,
    pid,
    flags
  );
}
#endif

#ifdef __NR_close_range
LSW_UNUSED ssize_t close_range(
    unsigned int fd,
    unsigned int max_fd,
    unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_close_range,
    fd,
    max_fd,
    flags
  );
}
#endif

#ifdef __NR_openat2
LSW_UNUSED ssize_t openat2(
    int dfd,
    const char * filename,
    struct open_how * how,
    size_t usize
) {
  return ::syscall::detail::syscall4(
    __NR_openat2,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    reinterpret_cast<ssize_t>(how),
    usize
  );
}
#endif

#ifdef __NR_pidfd_getfd
LSW_UNUSED ssize_t pidfd_getfd(
    int pidfd,
    int fd,
    unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_pidfd_getfd,
    pidfd,
    fd,
    flags
  );
}
#endif

#ifdef __NR_faccessat2
LSW_UNUSED ssize_t faccessat2(
    int dfd,
    const char * filename,
    int mode,
    int flags
) {
  return ::syscall::detail::syscall4(
    __NR_faccessat2,
    dfd,
    reinterpret_cast<ssize_t>(filename),
    mode,
    flags
  );
}
#endif

#ifdef __NR_process_madvise
LSW_UNUSED ssize_t process_madvise(
    int pidfd,
    const struct iovec * vec,
    size_t vlen,
    int behavior,
    unsigned int flags
) {
  return ::syscall::detail::syscall5(
    __NR_process_madvise,
    pidfd,
    reinterpret_cast<ssize_t>(vec),
    vlen,
    behavior,
    flags
  );
}
#endif

#ifdef __NR_epoll_pwait2
LSW_UNUSED ssize_t epoll_pwait2(
    int epfd,
    struct epoll_event * events,
    int maxevents,
    const struct __kernel_timespec * timeout,
    const sigset_t * sigmask,
    size_t sigsetsize
) {
  return ::syscall::detail::syscall6(
    __NR_epoll_pwait2,
    epfd,
    reinterpret_cast<ssize_t>(events),
    maxevents,
    reinterpret_cast<ssize_t>(timeout),
    reinterpret_cast<ssize_t>(sigmask),
    sigsetsize
  );
}
#endif

#ifdef __NR_mount_setattr
LSW_UNUSED ssize_t mount_setattr(
    int dfd,
    const char * path,
    unsigned int flags,
    struct mount_attr * uattr,
    size_t usize
) {
  return ::syscall::detail::syscall5(
    __NR_mount_setattr,
    dfd,
    reinterpret_cast<ssize_t>(path),
    flags,
    reinterpret_cast<ssize_t>(uattr),
    usize
  );
}
#endif

#ifdef __NR_quotactl_fd
LSW_UNUSED ssize_t quotactl_fd(
    unsigned int fd,
    unsigned int cmd,
    qid_t id,
    void * addr
) {
  return ::syscall::detail::syscall4(
    __NR_quotactl_fd,
    fd,
    cmd,
    id,
    reinterpret_cast<ssize_t>(addr)
  );
}
#endif

#ifdef __NR_landlock_create_ruleset
LSW_UNUSED ssize_t landlock_create_ruleset(
    const struct landlock_ruleset_attr *const attr,
    const size_t size,
    const unsigned int flags
) {
  return ::syscall::detail::syscall3(
    __NR_landlock_create_ruleset,
    reinterpret_cast<ssize_t>(attr),
    size,
    flags
  );
}
#endif

#ifdef __NR_landlock_add_rule
LSW_UNUSED ssize_t landlock_add_rule(
    const int ruleset_fd,
    const enum landlock_rule_type rule_type,
    const void *const rule_attr,
    const unsigned int flags
) {
  return ::syscall::detail::syscall4(
    __NR_landlock_add_rule,
    ruleset_fd,
    rule_type,
    reinterpret_cast<ssize_t>(rule_attr),
    flags
  );
}
#endif

#ifdef __NR_landlock_restrict_self
LSW_UNUSED ssize_t landlock_restrict_self(
    const int ruleset_fd,
    const unsigned int flags
) {
  return ::syscall::detail::syscall2(
    __NR_landlock_restrict_self,
    ruleset_fd,
    flags
  );
}
#endif

#ifdef __NR_memfd_secret
LSW_UNUSED ssize_t memfd_secret(
    unsigned int flags
) {
  return ::syscall::detail::syscall1(
    __NR_memfd_secret,
    flags
  );
}
#endif

#ifdef __NR_process_mrelease
LSW_UNUSED ssize_t process_mrelease(
    int pidfd,
    unsigned int flags
) {
  return ::syscall::detail::syscall2(
    __NR_process_mrelease,
    pidfd,
    flags
  );
}
#endif

#ifdef __NR_futex_waitv
LSW_UNUSED ssize_t futex_waitv(
    struct futex_waitv * waiters,
    unsigned int nr_futexes,
    unsigned int flags,
    struct __kernel_timespec * timeout,
    clockid_t clockid
) {
  return ::syscall::detail::syscall5(
    __NR_futex_waitv,
    reinterpret_cast<ssize_t>(waiters),
    nr_futexes,
    flags,
    reinterpret_cast<ssize_t>(timeout),
    clockid
  );
}
#endif

#ifdef __NR_set_mempolicy_home_node
LSW_UNUSED ssize_t set_mempolicy_home_node(
    unsigned long start,
    unsigned long len,
    unsigned long home_node,
    unsigned long flags
) {
  return ::syscall::detail::syscall4(
    __NR_set_mempolicy_home_node,
    start,
    len,
    home_node,
    flags
  );
}
#endif

/* VDSO */

#ifdef __NR_clock_getres
using clock_getres_t = ssize_t (
    const clockid_t which_clock,
    struct __kernel_timespec * tp
);

clock_getres_t* clock_getres;
#endif

#ifdef __NR_clock_gettime
using clock_gettime_t = ssize_t (
    const clockid_t which_clock,
    struct __kernel_timespec * tp
);

clock_gettime_t* clock_gettime;
#endif

#ifdef __NR_getcpu
using getcpu_t = ssize_t (
    unsigned int * cpup,
    unsigned int * nodep,
    struct getcpu_cache * unused
);

getcpu_t* getcpu;
#endif

#ifdef __NR_gettimeofday
using gettimeofday_t = ssize_t (
    struct __kernel_old_timeval * tv,
    struct timezone * tz
);

gettimeofday_t* gettimeofday;
#endif

#ifdef __NR_time
using time_t = ssize_t (
    __kernel_old_time_t * tloc
);

time_t* time;
#endif

namespace detail {

struct AuxvEntry {
    uint64_t type;
    uint64_t value;
};

struct VdsoInfo {
    unsigned char* base_addr;
    char* dynstr = nullptr;
    Elf64_Sym* dynsym = nullptr;
    size_t dynsym_entries = 0;
};

void* vdso_sym(::syscall::detail::VdsoInfo vdso, const char* sym_name, size_t sym_name_len) {
    void *func = NULL;

    for (int i = 0; i < vdso.dynsym_entries; i++) {
        Elf64_Sym& sym = vdso.dynsym[i];
        char* name = vdso.dynstr + sym.st_name;
        if (::syscall::detail::strncmp(name, sym_name, sym_name_len)) {
            func = vdso.base_addr + sym.st_value;
            break;
        }
    }

    return func;
}

} // namespace detail

void vdso_setup(int argc, char** argv) {
    // get to the end of envp
    char** envp = argv + argc + 1;
    while(*envp != nullptr) {
        ++envp;
    }

    // find the vdso_address
    ::syscall::detail::AuxvEntry* auxval = reinterpret_cast<::syscall::detail::AuxvEntry*>(envp + 1);
    while(auxval->type != AT_SYSINFO_EHDR) {
        auxval++;
    }

    ::syscall::detail::VdsoInfo vdso;
    vdso.base_addr = reinterpret_cast<unsigned char*>(auxval->value);

    // find dynstr/dynsym
    auto* elf_header = reinterpret_cast<Elf64_Ehdr*>(vdso.base_addr);
    auto* section_headers = reinterpret_cast<Elf64_Shdr*>(
        vdso.base_addr + elf_header->e_shoff
    );
    Elf64_Shdr& string_section_header = section_headers[elf_header->e_shstrndx];

    for (int i=0; i<elf_header->e_shnum; i++) {
        Elf64_Shdr& header = section_headers[i];
        auto* name = reinterpret_cast<char*>(
            vdso.base_addr + string_section_header.sh_offset + header.sh_name
        );
        if (::syscall::detail::strncmp(name, ".dynstr", sizeof(".dynstr") - 1)) {
            vdso.dynstr = reinterpret_cast<char*>(vdso.base_addr + header.sh_offset);
        }
        if (::syscall::detail::strncmp(name, ".dynsym", sizeof(".dynsym") - 1)) {
            vdso.dynsym = reinterpret_cast<Elf64_Sym*>(vdso.base_addr + header.sh_offset);
            vdso.dynsym_entries = header.sh_size / header.sh_entsize;
        }
    }

#ifdef __NR_clock_getres
    clock_getres = reinterpret_cast<::syscall::clock_getres_t*>(
        ::syscall::detail::vdso_sym(
            vdso,
            "clock_getres",
            sizeof("clock_getres") - 1
        )
    );
#endif

#ifdef __NR_clock_gettime
    clock_gettime = reinterpret_cast<::syscall::clock_gettime_t*>(
        ::syscall::detail::vdso_sym(
            vdso,
            "clock_gettime",
            sizeof("clock_gettime") - 1
        )
    );
#endif

#ifdef __NR_getcpu
    getcpu = reinterpret_cast<::syscall::getcpu_t*>(
        ::syscall::detail::vdso_sym(
            vdso,
            "getcpu",
            sizeof("getcpu") - 1
        )
    );
#endif

#ifdef __NR_gettimeofday
    gettimeofday = reinterpret_cast<::syscall::gettimeofday_t*>(
        ::syscall::detail::vdso_sym(
            vdso,
            "gettimeofday",
            sizeof("gettimeofday") - 1
        )
    );
#endif

#ifdef __NR_time
    time = reinterpret_cast<::syscall::time_t*>(
        ::syscall::detail::vdso_sym(
            vdso,
            "time",
            sizeof("time") - 1
        )
    );
#endif

}

} // namespace syscall

#endif
