import syscall;

extern "C" {

int main() {
  syscall::write(1, "test\n", 5);

  return 1;
}

}
